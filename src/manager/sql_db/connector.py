import sqlite3
import os
from configparser import ConfigParser
import colorama
from colorama import Fore, Back, Style
import numpy as np


# Class Object used to connect to the database
class Connector:

    # Initialization function
    def __init__(self, database=None):
        self.configFile = self.return_configFile()
        self.conn = None
        self.groundDir = None

        if database is True or database == 1:
            self.load()
        elif database is not None:
            self.load(database)

        print("Initialize colorama.")
        colorama.init()

    def load(self, database="default"):
        """
        ----------
        Paramaters
        ----------
        database : str
                   Name of the database. This acts as an id in the
                   sql_db/config.ini file.
        """
        # Find the file and check if it exists

        db = self.return_db_infos(database)
        if db is None:
            raise ValueError("Database do not exists.")
        self.groundDir = db["ground_dir"]

        # Name of the object, used for printing messages
        self.name = "sql_db.Connector"

        dataPath = "%s/%s.db" % (db["db_path"], database)
        try:
            self.conn = sqlite3.connect(dataPath)
        except sqlite3.OperationalError:
            raise sqlite3.OperationalError("unable to open database file. "
                                           "dataPath=" + dataPath)
        print("Opened database successfully!")

    def return_configFile(self):
        """Returns path to config.ini file."""
        filePath = os.path.dirname(os.path.abspath(__file__))
        configFile = "%s/config.ini" % filePath
        print(configFile)
        if not os.path.isfile(configFile):
            open(configFile, "a").close()
        return configFile

    def return_db_infos(self, db_name):
        """Returns the infos of db_name in the sql_db/config.ini file.
        If it does not exist, return None.
        ----------
        Parameters
        ----------
        db_name : str
                  Name of the database in the sql_db/config.ini file.
        """
        # Parse it and put data in db
        parser = ConfigParser()
        parser.read(self.configFile)
        db = {}
        if not parser.has_section(db_name):
            return None
        else:
            items = parser.items(db_name)
            for item in items:
                db[item[0]] = item[1]
            return db

    def mk_db_infos(self, infos):
        """
        ----------
        Paramaters
        ----------
        infos : dictionary
                Arguments: "name", "ground_dir", "db_path".
        """
        # Check the infos variable
        try:
            name = infos["name"]
            ground_dir, db_path = infos["ground_dir"], infos["db_path"]
        except Exception:
            raise Exception("In Connector.create_db(): infos needs to have "
                            "'name', 'ground_dir' and 'db_path'.")
        # Check whether database exists
        db = self.return_db_infos(name)
        if db is not None:
            raise ValueError("Cannot create this db_name as "
                             "it already exists.")

        # Create a new instance in manager/sql_db/config.ini.
        with open(self.configFile, 'a') as conf:
            conf.write("\n[%s]\nground_dir = %s\ndb_path = %s" %
                       (name, ground_dir, db_path))

    def rm_db_infos(self, db_name):
        parser = ConfigParser()
        with open(self.configFile, 'r') as conf:
            parser.readfp(conf)

        parser.remove_section(db_name)

        with open(self.configFile, "w") as conf:
            parser.write(conf)

    # Function used to act on the database. It opens the connection,
    # execute the request, closes the connection and returns the asked values.
    def print(self, _query):
        np.set_printoptions(threshold=np.inf, linewidth=np.inf)
        # Select query
        data = self.Query("select", _query)
        print(np.matrix(data))

    def select(self, _query):
        # Extracts data; returns array
        return self.Query("select", _query)

    def execute(self, _query):
        # Proposes action; commits
        self.Query("execute", _query)

    def insert(self, _query):
        # Proposes action; commits; returns primary key
        return self.Query("insert", _query)

    # Possible _type: "select", "execute", "insert".
    def Query(self, _type, _query, _args=()):
        conn = self.conn
        # Function's extansion for message prompting
        bar = "\t" + self.name + ".Query"
        print("%s%s: executing a %s request:%s" %
              (Fore.YELLOW, bar, _type, Style.RESET_ALL))
        # Print the query
        print("%s\t\t%s%s" % (Fore.YELLOW, _query, Style.RESET_ALL))

        # Type dependant actions
        if _type == "select":
            rows = conn.execute(_query)
            request = []
            for row in rows:
                request.append([])
                for r in row:
                    request[-1].append(r)
            return request     # Returns the selection

        elif _type == "execute":
            conn.execute(_query)
            conn.commit()   # Confirms the change

        elif _type == "insert":
            cursor = conn.execute(_query)
            conn.commit()
            # Takes the last id generated by auto_increment
            lastid = cursor.lastrowid
            return lastid   # Returns the last id
        else:
            print("%s\tERROR in %s: unrecognized type of request.%s" %
                  (Back.RED + Fore.BLACK, bar, Style.RESET_ALL))
            print()
