from Calculations.Calculation import Calculation
from Calculations.Barechi2.Barechi2 import Barechi2
from PostProd.PhysObj import PhysObj
from PostProd import KptMesh
from colorama import Fore, Back, Style

import pylab as pl
import re
import os
import subprocess


class Dressed(Calculation):

    def __init__(self, system, id=-1):
        Calculation.__init__(self, system)
        self.table = "dressed"
        self.prevTable = ["barechi2"]
#        self.prevTable = ["barechi2", "barechi1", "dmft"]
        self.prevCalc = Barechi2(system)

        if id != -1:
            self.id = id
            self.Load(True)

        self.params = {"dressedU": None, "dressedJ": None,
                       "miwn": None, "mivn": None}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)
        with open("%sInput.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        with open("%sIntParam.dat" % filePath, "r") as f:
            fileIntParam = f.read()

        def diffUJ():
            return str(float(self.params["dressedU"]) -
                       2*float(self.params["dressedJ"]))
        self.funcParams = {"diffUJ": diffUJ}

        self.treeDir = "U-$(dressedU)/J-$(dressedJ)/"

        self.tree = {"Input/": {"Input.dat": fileInput,
                                "%s.intparam" % system: fileIntParam,
                                "[%s.klist]" % system: ("&(barechi2.barechi2"
                                                        "dir)Input/%s.qlist" %
                                                        system),
                                "[%s.ReBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ReBareSusph" %
                                                              system),
                                "[%s.ImBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ImBareSusph" %
                                                              system)
                                },
                     "Output/": {},
                     }

    def SetEliash(self):
        if self.id == -1:
            print("%sError: You cannot plot from this calculation because"
                  " it is not defined in the database. Please load it"
                  " using self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.cells = self.Load(all=True)

        from Calculations.Eliash.Eliash import Eliash
        eliash = Eliash(self.system)
        eliash.prevId = self.id
        eliash.prevCalc = self

        return eliash

    def Launch(self):
        os.chdir(self.conn.groundDir + self.cells["dressed.dresseddir"] +
                 "Input/")
        print("%sExecuting BareVertex.py%s" % (Back.YELLOW + Fore.BLACK,
                                               Style.RESET_ALL))
        subprocess.call("python3 /mnt/parallel_scratch_mp2_wipe"
                        "_on_december_2017/"
                        "tremblay/gingras1/Programs/DressedChi/"
                        "BareVertex.py %s" % self.system, shell=True)

        os.chdir("..")
        print("%sExecuting DressedChi%s" % (Back.YELLOW + Fore.BLACK,
                                            Style.RESET_ALL))
        print("\t%sSaving output file in Dressed.out%s" % (Fore.YELLOW,
                                                           Fore.RESET))
        subprocess.call("/mnt/parallel_scratch_mp2_wipe_on_december_2017/"
                        "tremblay/gingras1/Programs/DressedChi/DressedChi"
                        "> Dressed.out", shell=True)

        print("\t%sFinding both Stoner factor from the file%s" % (Fore.YELLOW,
                                                                  Fore.RESET))
        with open("Dressed.out", "r") as f:
            first = True
            StonerD = 0
            StonerM = 0
            for line in f:
                if re.search("Stoner factor:", line):
                    if first:
                        StonerD = float(line.split()[4])
                        first = False
                    else:
                        StonerM = float(line.split()[4])
        print("\t%sStonerD is %f;\t StonerM is %f%s" % (Fore.MAGENTA, StonerD,
                                                        StonerM, Fore.RESET))
        self.conn.Query("execute", "UPDATE dressed SET stonerD = %f, stonerM ="
                        "%f WHERE dressedid = %d" % (StonerD, StonerM,
                                                     self.id))

    def ReturnGamma(self):
        print("Dressed.ReturnGamma: Returns the ph vertex function of"
              " this calculation in density (0) and magnetic (1) channels.")

        norb = self.cells["barechi2.norb"]
        Us = self.cells["dressed.dressedU"]
        Js = self.cells["dressed.dressedJ"]

        Ga = [pl.zeros((norb**2, norb**2)) for i in range(2)]

        for L1 in range(norb**2):
            l1 = int(L1 / norb)
            l2 = L1 % norb

            for L2 in range(norb**2):
                l3 = int(L2 / norb)
                l4 = L2 % norb

                if l1 == l2 and l1 == l3 and l1 == l4:
                    Ga[0][L1, L2] = Us
                    Ga[1][L1, L2] = Us
                elif l1 == l3 and l2 == l4 and l1 != l2:
                    Ga[0][L1, L2] = -Us + 4*Js
                    Ga[1][L1, L2] = Us - 2*Js
                elif l1 == l2 and l3 == l4 and l1 != l3:
                    Ga[0][L1, L2] = 2*Us - 5*Js
                    Ga[1][L1, L2] = Js
                elif l1 == l4 and l2 == l3 and l1 != l2:
                    Ga[0][L1, L2] = Js
                    Ga[1][L1, L2] = Js
                else:
                    Ga[0][L1, L2] = 0
                    Ga[1][L1, L2] = 0

        return Ga

    def ReturnBareSusc(self, nkpt):
        print("Dressed.ReturnBareSusc: Returns a PhysObj with the BareChi ph."
              " THIS SHOULD BE DONE IN BARECHI2.")

        norb = self.cells["barechi2.norb"]
        nfreq = 10

        Obj = PhysObj(norb**2, nkpt, nfreq)

        Obj.Read(self.conn.groundDir + self.baseDir +
                 'Input/Sr2RuO4.ReBareSusph', 2)

        return Obj.Obj

    def PlotDressedSus(self, nkpts, sgroup, toPlot="all", plotParams=None):
        if plotParams is None:
            plotParams = {}

        if self.id == -1:
            print("%sError: You cannot plot from this calcuylation because it"
                  " is not defined in the database. Please load it using"
                  " self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.cells = self.Load(all=True)

        Mesh = KptMesh(nkpts, _sgroup=sgroup)

        Mesh.read("%s%sU-%s/J-%s/Input/%s.klist" %
                  (self.conn.groundDir, self.baseDir,
                   str(self.cells["dressed.dressedU"]),
                   str(self.cells["dressed.dressedJ"]), self.system))

        if self.cells['barechi2.norb'] != 3:
            print("%sError: norb is %d, case not worked out yet.%s" %
                  (Fore.BLACK + Back.RED, self.cells["barechi2.norb"],
                   Style.RESET_ALL))
            return False

        if toPlot == "all":
            toPlot = {}
            toPlot["ext"] = ["Density", "Magnetic"]
            toPlot["mivn"] = range(self.cells["dressed.mivn"]+1)

        import pylab as pl
        import mpl_toolkits.axes_grid1.inset_locator as inset_axes

        nnorb = self.cells["barechi2.norb"]**2

        ll = [[0, 0], [4, 4], [8, 8]]
        llabels = [r"\alpha", r"\beta", r"\gamma"]

        for ext in toPlot["ext"]:
            ReObj = PhysObj(nnorb, Mesh.nkpt, self.cells["dressed.mivn"] + 1)
            ReObj.Read("%s%sU-%s/J-%s/Output/%s.ReDressedSusph%s" %
                       (self.conn.groundDir, self.baseDir,
                        str(self.cells["dressed.dressedU"]),
                        str(self.cells["dressed.dressedJ"]),
                        self.system, ext), _skip=2)

            ImObj = PhysObj(nnorb, Mesh.nkpt, self.cells["dressed.mivn"] + 1)
            ImObj.Read("%s%sU-%s/J-%s/Output/%s.ImDressedSusph%s" %
                       (self.conn.groundDir, self.baseDir,
                        str(self.cells["dressed.dressedU"]),
                        str(self.cells["dressed.dressedJ"]),
                        self.system, ext), _skip=2)

            if ext == "Density":
                tag = "d"
            elif ext == "Magnetic":
                tag = "m"

            for m in toPlot["mivn"]:
                print("")
                fig, ax = pl.subplots(1, self.cells["barechi2.norb"],
                                      figsize=(8, 4))
                for l in range(len(ll)):

                    Real = ReObj.ReturnInPlane(Mesh, [ll[l], m])
                    print("Real: \t Min: %f \t Max: %f" % (pl.amin(Real),
                                                           pl.amax(Real)))

                    Reimg = ax[l].imshow(Real, origin="lower",
                                         cmap=pl.get_cmap("bwr"))

                    ax[l].set_title(r"$\Re\{\chi^%s_{ph}(\vec{q}, i\omega_"
                                    r"{ %s })_{ %s, %s} \}$" %
                                    (tag, m, llabels[l], llabels[l]))
                    ax[l].set_xticks([], [])
                    ax[l].set_yticks([], [])
                    ax[l].set_xlim([0, nkpts[0] - 1])
                    ax[l].set_ylim([0, nkpts[1] - 1])

                    Reinset_ax = inset_axes.inset_axes(ax[l], width="3%",
                                                       height="85%", loc=2)
                    Recbar = pl.colorbar(Reimg, cax=Reinset_ax,
                                         format="%01.01e")
                    Recbar.ax.tick_params(labelsize=10)

                    Recbar.set_ticks([pl.amin(Real), pl.amax(Real)])

                    r'''
                    Imag = ImObj.ReturnInPlane(Mesh, [ll[l], m])
                    print("Imag: \t Min: %f \t Max: %f" %
                          (pl.amin(Imag), pl.amax(Imag)))

                    Imimg = ax[1, l].imshow(Imag, origin="lower",
                                            cmap = pl.get_cmap("bwr"))

                    ax[1, l].set_title(r"$\Im\{\chi^%s_{ph}(\vec{q},"
                                       r"i\omega_{ %s })_{ %s, %s} \}$" %
                                       (tag, m, llabels[l], llabels[l]))
                    ax[1, l].set_xticks([], [])
                    ax[1, l].set_yticks([], [])
                    ax[1, l].set_xlim([0, nkpts[0] - 1])
                    ax[1, l].set_ylim([0, nkpts[1] - 1])

                    Iminset_ax = inset_axes.inset_axes(ax[1, l], width = "3%",
                                                       height = "85%", loc = 2)
                    Imcbar = pl.colorbar(Imimg, cax=Iminset_ax,
                                         format="%01.01e")
                    Imcbar.ax.tick_params(labelsize=10)

                    Imcbar.set_ticks([pl.amin(Imag),pl.amax(Imag)])
                    '''

                savename = ("%s%sU-%s/J-%s/%s_w-%d.png" %
                            (self.conn.groundDir, self.baseDir,
                             str(self.cells["dressed.dressedU"]),
                             str(self.cells["dressed.dressedJ"]), ext, m))

                print("Save figure: %s" % savename)
                pl.savefig(savename)
#                pl.show()
                pl.draw()
