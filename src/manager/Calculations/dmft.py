from .calculation import Calculation
from manager.PostProd import PhysObj, Gamma, PlotParams, TwoPropagator
from manager.PostProd import plotting
from colorama import Fore, Back, Style
from itertools import product as itp
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import matplotlib.pyplot as plt
import re
import os
import itertools


class DMFT(Calculation):

    def __init__(self, system, conn, id=-1):
        Calculation.__init__(self, system, conn)
        self.table = "dmft"

        if id != -1:
            self.id = id
            self.Load(True)

        self.params = {"UK": None, "JK": None,
                       "beta": None, "magnetic": None,
                       "solver": None, "w90_in": None}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)
        with open("%s/DMFT/Input.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        self.treeDir = ("U-$(dressedU)_J-$(dressedJ)_Up-$(dressedUp)_"
                        "Upp-$(dressedUpp)_Jp-$(dressedJp)/")
        self.treeDir = ("b$(beta)-U$(U)-J$(J)/")

        self.tree = {"dmft_config.ini": fileInput,
                     "[%s.h5]" % system: ("$(w90_in)"),
                    }

    def CreateTable(self):
        self.testConn()

        query = ("CREATE TABLE dmft ( "
                 "dmftid INTEGER NOT NULL PRIMARY KEY, "
                 "dmftdir CHAR(255) NOT NULL, "
                 "beta REAL NOT NULL, "
                 "U REAL NOT NULL, "
                 "J REAL NOT NULL, "
                 "w90_in CHAR(255) NOT NULL, "
                 "parameters TEXT NOT NULL, "
                 "magnetic CHAR(5), "
                 "solver CHAR(255), "
                 "status CHAR(255))")
        self.conn.Query("execute", query)

    def InsertNew(self, values):
        """Insert new calculation in the dmft table."""
        self.testConn()

        keys = ["dmftid", "dmftdir", "beta", "U", "J", "w90_in", "parameters",
                "magnetic", "solver", "status"]
        txt_keys, txt_values = "(", "("
        for v, val in enumerate(values):
            if val not in keys:
                raise Exception("Unrecognized key '%s'." % val)
            if v != 0:
                txt_keys += ", "
                txt_values += ", "
            txt_keys += val
            if isinstance(values[val], str):
                txt_values += "'%s'" % values[val]
            else:
                txt_values += str(values[val])
        txt_keys += ")"
        txt_values += ")"

        query = ("INSERT INTO dmft %s VALUES %s" % (txt_keys, txt_values))
        print(query)
        self.id = self.conn.Query("insert", query)
        print("self.id: ", self.id)
        self.Load(True)
