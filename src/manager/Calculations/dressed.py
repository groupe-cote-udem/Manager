from .calculation import Calculation
from .barechi2 import Barechi2
from manager.PostProd import PhysObj, Gamma, PlotParams, TwoPropagator
from manager.PostProd import plotting
from colorama import Fore, Back, Style
from itertools import product as itp
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import matplotlib.pyplot as plt
import re
import os
# import subprocess
import itertools


class Dressed(Calculation):

    def __init__(self, system, conn, id=-1):
        Calculation.__init__(self, system, conn)
        self.table = "dressed"
        self.prevTable = ["barechi2"]
#        self.prevTable = ["barechi2", "barechi1", "dmft"]
        self.prevCalc = Barechi2(system, conn)

        self.gammaph = None

        if id != -1:
            self.id = id
            self.Load(True)

        self.params = {"dressedU": None, "dressedJ": None,
                       "dressedUp": None, "dressedUpp": None,
                       "dressedJp": None, "miwn": None, "mivn": None}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)
        with open("%s/Dressed/Input.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        with open("%s/Dressed/IntParam.dat" % filePath, "r") as f:
            fileIntParam = f.read()

        def diffUJ():
            return str(float(self.params["dressedU"]) -
                       2*float(self.params["dressedJ"]))
        self.funcParams = {"diffUJ": diffUJ}

        self.treeDir = ("U-$(dressedU)_J-$(dressedJ)_Up-$(dressedUp)_"
                        "Upp-$(dressedUpp)_Jp-$(dressedJp)/")

        self.tree = {"Input/": {"Input.dat": fileInput,
                                "%s.intparam" % system: fileIntParam,
                                "[%s.klist]" % system: ("&(barechi2.barechi2"
                                                        "dir)Input/%s.qlist" %
                                                        system),
                                "[%s.ReBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ReBareSusph" %
                                                              system),
                                "[%s.ImBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ImBareSusph" %
                                                              system)
                                },
                     "Output/": {},
                     }

    def CreateTable(self):
        self.testConn()

        query = ("CREATE TABLE dressed ( "
                 "dressedid INTEGER NOT NULL PRIMARY KEY, "
                 "barechi2id INTEGER, "
                 "dressedU REAL NOT NULL, "
                 "dressedJ REAL NOT NULL, "
                 "dressedUp REAL, "
                 "dressedJp REAL, "
                 "dressedUpp REAL, "
                 "miwn INTEGER, "
                 "mivn INTEGER, "
                 "dresseddir CHAR(255) NOT NULL, "
                 "stoner REAL, "
                 "stonerD REAL, "
                 "stonerM REAL, "
                 "status CHAR(255))")
        self.conn.Query("execute", query)

    def PrintDressingInfos(self):
        '''
        Function in order to get whole information-set on a dressed calc.
        '''

        U = self.cells["dressed.dressedU"]
        J = self.cells["dressed.dressedJ"]
        sM = self.cells["dressed.stonerM"]
        b2id = self.cells["barechi2.barechi2id"]
        did = self.id

        print("""Final parameters for this dressing calculation were :\n
                 DressedU = %0.8f\n
                 DressedJ = %0.8f\n
                 J/U = %0.4f\n
                 Magnetic Stoner factor = %0.5f\n
                 dressedid = %d\n
                 barechi2id = %d)""" % (U, J, J/U, sM, did, b2id))

    def SetEliash(self, nspin=1, ndim=1, pseudo=0):
        if self.id == -1:
            print("%sError: You cannot plot from this calculation because"
                  " it is not defined in the database. Please load it"
                  " using self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.cells = self.Load(all=True)

        from .eliash import Eliash
        eliash = Eliash(self.system, self.conn, nspin=nspin, ndim=ndim,
                        pseudo=pseudo)
        eliash.prevId = self.id
        eliash.prevCalc = self

        return eliash

    def SetGammaph(self):
        U = self.cells["dressed.dressedU"]
        Up = self.cells["dressed.dressedUp"]
        Upp = self.cells["dressed.dressedUpp"]
        J = self.cells["dressed.dressedJ"]
        Jp = self.cells["dressed.dressedJp"]

        norb = self.cells["barechi2.norb"]

        if Jp is None:
            Jp = J
        if Up is None:
            Up = U-2*J
        if Upp is None:
            Upp = Up - J

        U, Up, Upp = round(U, 10), round(Up, 10), round(Upp, 10)
        J, Jp = round(J, 10), round(Jp, 10)

        gamma = Gamma(norb, U, J, Up=Up, Upp=Upp, Jp=Jp, spindiag=False)
        gamma.constructRPA()
        gamma.checkCrossingSym()

        self.gammaph = gamma

    def Dressing(self, nkpt=None, chi0=None, flag_nostop=False):
        susDir = (self.conn.groundDir +
                  self.cells["barechi2.barechi2dir"])
        dreDir = self.conn.groundDir + self.cells["dressed.dresseddir"]
        nspin = self.cells["barechi2.nspin"]
        norb = self.cells["barechi2.norb"]
        nivn = self.cells["barechi2.nivn"]
        nivn += nivn % 2

        self.SetGammaph()
        gamma = self.gammaph

        if chi0 is None:
            if nkpt is None:
                raise ValueError("If chi0 is None, nkpt must be specified.")
            chi0 = TwoPropagator(nspin*norb, nivn + 1, nkpt, spindiag=False)
            chi0.Re.Read(susDir + "Output/%s.ReBareSusph" % self.system,
                         _skip=2)
            chi0.Im.Read(susDir + "Output/%s.ImBareSusph" % self.system,
                         _skip=2)

        dchi, eig0 = chi0.dressing(gamma, returnEig0=True,
                                   flag_nostop=flag_nostop)
        realtop = ("# Bosonic Freq.,  q index\n"
                   "# ReDressedSusph(index1, index2) where index1, index2 are "
                   "composite orbitals and spins. U=%s; J=%s"
                   % (str(gamma.U), str(gamma.J)))
        imagtop = ("# Bosonic Freq.,  q index\n"
                   "# ImDressedSusph(index1, index2) where index1, index2 are "
                   "composite orbitals and spins. U=%s; J=%s"
                   % (str(gamma.U), str(gamma.J)))
        dchi.Re.write(dreDir + "Output/%s.ReDressedSusph" % self.system,
                      realtop)
        dchi.Im.write(dreDir + "Output/%s.ImDressedSusph" % self.system,
                      imagtop)

        # TO DO: MOVE THIS TO GAMMA OBJECT?
        inttop = ("#  Gamma^0(12;34)  where i=(atom index, orbital index, "
                  "spin index). U=%s; J=%s\n"
                  "#     3up --->------->--- 1up\n"
                  "#                 |\n"
                  "#     4up ---<-------<--- 2up\n"
                  "#\n" % (str(gamma.U), str(gamma.J)))
        with open(dreDir + "Input/%s.BareIntph" % self.system, "w") as intf:
            intf.write(inttop)
            text = ""
            for l1 in range(nspin**2*norb**2):
                for l2 in range(nspin**2*norb**2):
                    text += "\t%.6f" % gamma.gamma[l1, l2]
                text += "\n"
            intf.write(text)

        idx = eig0[:, 0, 0].argsort()[::-1]
        StonerT = eig0[idx, 0, 0]
        StonerT = StonerT[0]
        print(StonerT)
        print("\t%sTotal Stoner factor is %f and happends at k-point number "
              "%d.%s" % (Fore.MAGENTA, StonerT, idx[0], Fore.RESET))

        idx = eig0[:, 0, 1].argsort()[::-1]
        StonerD = eig0[idx, 0, 1]
        StonerD = StonerD[0]
        print("\t%sDensity Stoner factor is %f and happends at k-point number "
              "%d.%s" % (Fore.MAGENTA, StonerD, idx[0], Fore.RESET))

        idx = eig0[:, 0, 2].argsort()[::-1]
        StonerM = eig0[idx, 0, 2]
        StonerM = StonerM[0]
        print("\t%sMagnetic Stoner factor is %f and happends at k-point number"
              " %d.%s" % (Fore.MAGENTA, StonerM, idx[0], Fore.RESET))

        self.conn.Query("execute", "UPDATE dressed SET stoner=%f, "
                        "stonerD=%f, stonerM =%f WHERE dressedid = %d"
                        % (StonerT, StonerD, StonerM, self.id))
        return eig0

    def ReturnDressedSusph(self, nkpt):
        if self.id == -1:
            raise ValueError("Error: No id.")

        susDir = self.conn.groundDir + self.cells["dressed.dresseddir"]
        norb = self.cells["barechi2.norb"]
        nspin = self.cells["barechi2.nspin"]
        nivn = self.cells["barechi2.nivn"]

        if nspin == 1:
            dchi_d = TwoPropagator(nspin*norb, nivn + 1, nkpt, spindiag=True)
            dchi_d.Re.Read("%sOutput/%s.ReDressedSusphDensity" %
                           (susDir, self.system), _skip=2)
            dchi_d.Im.Read("%sOutput/%s.ImDressedSusphDensity" %
                           (susDir, self.system), _skip=2)
            dchi_m = TwoPropagator(nspin*norb, nivn + 1, nkpt, spindiag=True)
            dchi_m.Re.Read("%sOutput/%s.ReDressedSusphMagnetic" %
                           (susDir, self.system), _skip=2)
            dchi_m.Im.Read("%sOutput/%s.ImDressedSusphMagnetic" %
                           (susDir, self.system), _skip=2)
            dchi = [dchi_d, dchi_m]
        elif nspin == 2:
            dchi = TwoPropagator(nspin*norb, nivn + 1, nkpt, spindiag=False)
            dchi.Re.Read("%sOutput/%s.ReDressedSusph" % (susDir, self.system),
                         _skip=2)
            dchi.Im.Read("%sOutput/%s.ImDressedSusph" % (susDir, self.system),
                         _skip=2)
        else:
            raise ValueError("nspin != [0, 1] not implemented.")

        return dchi

    def ReturnLadderph(self, nkpt):
        if self.gammaph is None:
            raise Exception("The gammaph variable of the dressing object "
                            "is not set. Use .SetGammaph.")
        gamma = self.gammaph.gamma
        chiph = self.ReturnDressedSusph(nkpt)
        chiph = chiph.Re.Obj + 1j*chiph.Im.Obj

        norb = self.cells["barechi2.norb"]
        nspin = self.cells["barechi2.nspin"]
        ndim = norb*nspin
        nfreq = self.cells["dressed.mivn"] + 1

        ladder = np.zeros((nfreq, nkpt, ndim**2, ndim**2), dtype=complex)

        for v, q in itp(range(nfreq), range(nkpt)):
            ladder[v, q] = gamma @ chiph[q, v] @ gamma

        return ladder

    def ReturnLadderpp(self, nkpt, nv, gammaph, ladderph=None):
        if ladderph is None:
            ladderph = self.ReturnLadderph(nkpt)
        nspin, norb = 2, 3
        ndim = norb*nspin

        # We will now construct Gamma^0_pp, Phi^1_pp and Phi^2_pp
        # They are obtain by rotatng from the ph to the pp channel
        # We need them both for positive and negative frequencies
        gammapp0 = np.zeros((ndim**2, ndim**2), dtype=complex)
        ladderpp1 = np.zeros((2*nv-1, nkpt, ndim**2, ndim**2), dtype=complex)
        ladderpp2 = np.zeros((2*nv-1, nkpt, ndim**2, ndim**2), dtype=complex)
        for m1, m2, m3, m4 in itp(range(ndim), repeat=4):
            gammapp0[m1*ndim+m2, m3*ndim+m4] = -gammaph[m1*ndim+m4, m3*ndim+m2]
            for v in range(2*nv-1):
                tmp_v = v - nv + 1
                mm1, mm2 = m1*ndim+m2, m3*ndim+m4
                if tmp_v < 0:
                    tmp_v2 = -1*tmp_v
                    if m1 == 0 and m2 == 0 and m3 == 0 and m4 == 0:
                        print("v", v, "; tmp_v2", tmp_v2)
                    tmp_lad1 = ladderph[tmp_v2, :, m3*ndim+m1, m2*ndim+m4]
                    ladderpp1[v, :, mm1, mm2] = np.conj(tmp_lad1)
                    tmp_lad2 = ladderph[tmp_v2, :, m3*ndim+m2, m1*ndim+m4]
                    ladderpp2[v, :, mm1, mm2] = np.conj(tmp_lad2)
                else:
                    if m1 == 0 and m2 == 0 and m3 == 0 and m4 == 0:
                        print("v", v, "; tmp_v", tmp_v)
                    tmp_lad1 = ladderph[tmp_v, :, m2*ndim+m4, m3*ndim+m1]
                    ladderpp1[v, :, mm1, mm2] = tmp_lad1
                    tmp_lad2 = ladderph[tmp_v, :, m1*ndim+m4, m3*ndim+m2]
                    ladderpp2[v, :, mm1, mm2] = tmp_lad2
        return gammapp0, ladderpp1, ladderpp2

    def ConstructTwoFreqGammapp(self, freqs, nkpt, gammapp0,
                                ladderpp1, ladderpp2):
        norb, nspin = 3, 2
        ndim = norb*nspin
        nv, nw = freqs["nv"], freqs["nw"]
        # nv, nw, nw_chi = freqs["nv"], freqs["nw"], freqs["nw_chi"]
        # #########################################
        # Calculate the diagonal version for parity
        # #########################################
        Gpp0 = np.zeros((ndim**2, ndim**2), dtype=complex)
        Gpp1 = np.zeros((nkpt, 2*nw, 2*nw, ndim**2, ndim**2), dtype=complex)
        Gpp2 = np.zeros((nkpt, 2*nw, 2*nw, ndim**2, ndim**2), dtype=complex)

        Gpp0 = gammapp0

        # First we need to right frequency labeling
        for q, w1, w2 in itp(range(nkpt), range(2*nw), range(2*nw)):
            # wn goes in {-wmax+1,..., -0,+0, ..., wmax+1} tp {0, ..., 2wmax-1}
            # vn in {-vmax+1, ..., -1,0, 1, ..., vmax-1} to {0, ..., 2(vmax-1)}
            #
            # example wn in {-0, +0} to {0, 1} (wmax = 1) and
            #  vn in {-2, ..., 2} to {0, ..., 4} (vmax = 3)
            #
            #  with wms = -0, -0, then ladder+ is vn = -1 and ladder- is vn = 0
            #  which goes to v[vn + vmax - 1] thus v[1] and v[2]
            wm1, wm2 = w1 - nw + 1/2, w2 - nw + 1/2
            w_p, w_m = wm2 + wm1, wm2 - wm1
            v_p, v_m = int(w_p + nv - 1), int(w_m + nv - 1)

            if q == 0:
                print("\nwm1", wm1, "; wm2", wm2, "; w_p", w_p, "; w_m", w_m,
                      "v_p", v_p, "; v_m", v_m)

            Gpp1[q, w1, w2] = -ladderpp1[v_m, q]
            Gpp2[q, w1, w2] = +ladderpp2[v_p, q]
        return Gpp0, Gpp1, Gpp2

    def EffectiveGammapp(self, Gpp0, Gpp1, Gpp2, nw=1):
        norb = 3

        # Then we rotate to the right combinaison
        n1, n2 = norb**2, 2*norb**2

        G0pmpm = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0pmmp = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0mppm = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0mpmp = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)

        G1pmpm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1pmmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1mppm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1mpmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)

        G2pmpm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2pmmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2mppm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2mpmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)

        minus_w = [x for x in np.arange(2*nw-1, -1, -1)]
        print("minus_w:", minus_w)
        for l1, l2, l3, l4 in itp(range(norb), repeat=4):
            l12, l21, l34, l43 = 9+l1*3+l2, 18+l2*3+l1, 9+l3*3+l4, 18+l4*3+l3
            ll1, ll2 = l1*3+l2, l3*3+l4

            G0pmpm[ll1, ll2] = Gpp0[l12, l34]
            G0pmmp[ll1, ll2] = Gpp0[l12, l43]
            G0mppm[ll1, ll2] = Gpp0[l21, l34]
            G0mpmp[ll1, ll2] = Gpp0[l21, l43]

            G1pmpm[:, :, :, ll1, ll2] = Gpp1[:, :, :, l12, l34]
            G1pmmp[:, :, :, ll1, ll2] = Gpp1[:, :, minus_w][:, :, :, l12, l43]
            G1mppm[:, :, :, ll1, ll2] = Gpp1[:, minus_w][:, :, :, l21, l34]
            tmp_G = Gpp1[:, minus_w][:, :, minus_w][:, :, :, l21, l43]
            G1mpmp[:, :, :, ll1, ll2] = tmp_G

            G2pmpm[:, :, :, ll1, ll2] = Gpp2[:, :, :, l12, l34]
            G2pmmp[:, :, :, ll1, ll2] = Gpp2[:, :, minus_w][:, :, :, l12, l43]
            G2mppm[:, :, :, ll1, ll2] = Gpp2[:, minus_w][:, :, :, l21, l34]
            tmp_G = Gpp2[:, minus_w][:, :, minus_w][:, :, :, l21, l43]
            G2mpmp[:, :, :, ll1, ll2] = tmp_G

        print("######### G0 ########")
        print(np.absolute(Gpp0))

        effG0 = 0.5*np.block([[G0pmpm - G0mppm - G0pmmp + G0mpmp,
                               G0pmpm - G0mppm + G0pmmp - G0mpmp],
                              [G0pmpm + G0mppm - G0pmmp - G0mpmp,
                               G0pmpm + G0mppm + G0pmmp + G0mpmp]])
        effG1 = 0.5*np.block([[G1pmpm - G1mppm - G1pmmp + G1mpmp,
                               G1pmpm - G1mppm + G1pmmp - G1mpmp],
                              [G1pmpm + G1mppm - G1pmmp - G1mpmp,
                               G1pmpm + G1mppm + G1pmmp + G1mpmp]])
        effG2 = 0.5*np.block([[G2pmpm - G2mppm - G2pmmp + G2mpmp,
                               G2pmpm - G2mppm + G2pmmp - G2mpmp],
                              [G2pmpm + G2mppm - G2pmmp - G2mpmp,
                               G2pmpm + G2mppm + G2pmmp + G2mpmp]])

        return effG0, effG1, effG2

    def EffectiveGammapp2(self, Gpp0, Gpp1, Gpp2, nw=1):
        norb = 3

        # Then we rotate to the right combinaison
        n1, n2 = norb**2, 2*norb**2

        G0pmpm = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0pmmp = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0mppm = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)
        G0mpmp = np.zeros(Gpp0[n1:n2, n1:n2].shape, dtype=complex)

        G1pmpm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1pmmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1mppm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G1mpmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)

        G2pmpm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2pmmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2mppm = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)
        G2mpmp = np.zeros(Gpp1[:, :, :, n1:n2, n1:n2].shape, dtype=complex)

        minus_w = [x for x in np.arange(2*nw-1, -1, -1)]
        print("minus_w:", minus_w)
        for l1, l2, l3, l4 in itp(range(norb), repeat=4):
            l12, l21, l34, l43 = 9+l1*3+l2, 18+l2*3+l1, 9+l3*3+l4, 18+l4*3+l3
            ll1, ll2 = l1*3+l2, l3*3+l4

            G0pmpm[ll1, ll2] = Gpp0[l12, l34]
            G0pmmp[ll1, ll2] = Gpp0[l12, l43]
            G0mppm[ll1, ll2] = Gpp0[l21, l34]
            G0mpmp[ll1, ll2] = Gpp0[l21, l43]

            G1pmpm[:, :, :, ll1, ll2] = Gpp1[:, :, :, l12, l34]
            G1pmmp[:, :, :, ll1, ll2] = Gpp1[:, :, minus_w][:, :, :, l12, l43]
            G1mppm[:, :, :, ll1, ll2] = Gpp1[:, minus_w][:, :, :, l21, l34]
            tmp_G = Gpp1[:, minus_w][:, :, minus_w][:, :, :, l21, l43]
            G1mpmp[:, :, :, ll1, ll2] = tmp_G

            G2pmpm[:, :, :, ll1, ll2] = Gpp2[:, :, :, l12, l34]
            G2pmmp[:, :, :, ll1, ll2] = Gpp2[:, :, minus_w][:, :, :, l12, l43]
            G2mppm[:, :, :, ll1, ll2] = Gpp2[:, minus_w][:, :, :, l21, l34]
            tmp_G = Gpp2[:, minus_w][:, :, minus_w][:, :, :, l21, l43]
            G2mpmp[:, :, :, ll1, ll2] = tmp_G

        print("######### G0 ########")
        print(np.absolute(Gpp0))

        effG0_1 = 0.5*np.block([[G0pmpm - G0mppm, G0pmpm - G0mppm],
                                [G0pmpm + G0mppm, G0pmpm + G0mppm]])
        effG0_2 = 0.5*np.block([[-G0pmmp + G0mpmp, G0pmmp - G0mpmp],
                                [-G0pmmp - G0mpmp, G0pmmp + G0mpmp]])
        effG1_1 = 0.5*np.block([[G1pmpm - G1mppm, G1pmpm - G1mppm],
                                [G1pmpm + G1mppm, G1pmpm + G1mppm]])
        effG1_2 = 0.5*np.block([[-G1pmmp + G1mpmp, G1pmmp - G1mpmp],
                                [-G1pmmp - G1mpmp, G1pmmp + G1mpmp]])
        effG2_1 = 0.5*np.block([[G2pmpm - G2mppm, G2pmpm - G2mppm],
                                [G2pmpm + G2mppm, G2pmpm + G2mppm]])
        effG2_2 = 0.5*np.block([[-G2pmmp + G2mpmp, G2pmmp - G2mpmp],
                                [-G2pmmp - G2mpmp, G2pmmp + G2mpmp]])

        return effG0_1, effG0_2, effG1_1, effG1_2, effG2_1, effG2_2

    def WriteChipp(self, nkpt, freqs, write_path, bchipp=None,
                   verbose=False, p_and_m="pm"):
        norb, nw_chi = 3, freqs["nw_chi"]
        if bchipp is None:
            b = Barechi2(self.system, self.conn,
                         id=self.cells["barechi2.barechi2id"])
            bchipp = b.ReturnBareChipp(nkpt)

        chipp0 = np.copy(bchipp.Re.Obj + 1j*bchipp.Im.Obj)
        # Going to the pseudospin basis
        A, B = bchipp.changeBasis(chipp0, basis1="OSOS", basis2="OOPP")
        chipp0[:, :, A, :] = chipp0[:, :, B, :]
        chipp0[:, :, :, A] = chipp0[:, :, :, B]

        if verbose:
            # Check that chipp is almost pseudospin diagonal
            print("##########")
            print("\nchi^0_pp in pseudospin basis, max")
            print("##########")
            max_chipp = np.zeros((4, 4))
            for i, j in itp(range(4), repeat=2):
                i1, i2 = norb**2*i, norb**2*(i+1)
                j1, j2 = norb**2*j, norb**2*(j+1)
                tmp_chi = chipp0[:, :, i1:i2, j1:j2]
                max_chi = np.amax(np.absolute(tmp_chi))
                max_chipp[i, j] = max_chi
            print("Max of each pseudospin component:")
            print(max_chipp)

        if p_and_m == "pm":
            chi = chipp0[:, :, norb**2:2*norb**2, norb**2:2*norb**2]
            norb2 = norb**2
            ext = "_pm"
        elif p_and_m == "pp":
            chi = chipp0[:, :, :norb**2, :norb**2]
            norb2 = norb**2
            ext = "_pp"
        elif p_and_m == "pm_mp":
            chi = chipp0[:, :, norb**2:3*norb**2, norb**2:3*norb**2]
            norb2 = 2*norb**2
            ext = "_pm_mp"

        chi_Re = PhysObj(norb2, nkpt, nw_chi)
        chi_Im = PhysObj(norb2, nkpt, nw_chi)
        chi_Re[:] = chi.real[:]
        chi_Im[:] = chi.imag[:]

        # Write it
        top = ("# Bosonic freq., q index\n"
               "# %sBareChipp%s(index1, index2) where index1, index2 are "
               "are orbitals for composite orbital with composite "
               "pseudospin +-, -+")
        chi_Re.write("%s%s.ReBareChipp%s" % (write_path, self.system, ext),
                     top % ("Re", ext))
        chi_Im.write("%s%s.ImBareChipp%s" % (write_path, self.system, ext),
                     top % ("Im", ext))

    # def WriteChipp_pm_mp(self, nkpt, freqs, write_path, bchipp=None,
    #                      verbose=False):
    #     norb, nw_chi = 3, freqs["nw_chi"]
    #     if bchipp is None:
    #         b = Barechi2(self.system, self.conn,
    #                      id=self.cells["barechi2.barechi2id"])
    #         bchipp = b.ReturnBareChipp(nkpt)

    #     chipp0 = np.copy(bchipp.Re.Obj + 1j*bchipp.Im.Obj)
    #     # Going to the pseudospin basis
    #     A, B = bchipp.changeBasis(chipp0, basis1="OSOS", basis2="OOPP")
    #     chipp0[:, :, A, :] = chipp0[:, :, B, :]
    #     chipp0[:, :, :, A] = chipp0[:, :, :, B]

    #     if verbose:
    #         # Check that chipp is almost pseudospin diagonal
    #         print("##########")
    #         print("\nchi^0_pp in pseudospin basis, max")
    #         print("##########")
    #         max_chipp = np.zeros((4, 4))
    #         for i, j in itp(range(4), repeat=2):
    #             i1, i2 = norb**2*i, norb**2*(i+1)
    #             j1, j2 = norb**2*j, norb**2*(j+1)
    #             tmp_chi = chipp0[:, :, i1:i2, j1:j2]
    #             max_chi = np.amax(np.absolute(tmp_chi))
    #             max_chipp[i, j] = max_chi
    #         print("Max of each pseudospin component:")
    #         print(max_chipp)

    #     # Select only the +- component
    #     chi_pm_mp = chipp0[:, :, norb**2:3*norb**2, norb**2:3*norb**2]
    #     chi_pm_mp_Re = PhysObj(2*norb**2, nkpt, nw_chi)
    #     chi_pm_mp_Im = PhysObj(2*norb**2, nkpt, nw_chi)
    #     chi_pm_mp_Re[:] = chi_pm_mp.real[:]
    #     chi_pm_mp_Im[:] = chi_pm_mp.imag[:]

    #     # Write it
    #     top = ("# Bosonic freq., q index\n"
    #            "# %sBareChipp_pm(index1, index2) where index1, index2 are ")
    #     chi_pm_mp_Re.write("%s.ReBareChipp_pm_mp" % write_path, top % "Re")
    #     chi_pm_mp_Im.write("%s.ImBareChipp_pm_mp" % write_path, top % "Im")

    # def PreparePseudospinEffectivePairing2(self, nkpt, freqs,
    #                                        write_path=None,
    #                                        verbose=False):
    #     norb = 3
    #     nw, nv = freqs["nw"], freqs["nv"]

    #     # Gamma^0_ph and Phi_ph
    #     self.SetGammaph()
    #     ladderph = self.ReturnLadderph(nkpt)
    #     gammaph = self.gammaph.gamma

    #     # Rotate to the pp channel
    #     gammapp0, ladderpp1, ladderpp2 = (self.ReturnLadderpp(nkpt, nv,
    #                                       gammaph, ladderph))

    #     b = Barechi2(self.system, self.conn,
    #                  id=self.cells["barechi2.barechi2id"])
    #     bchipp = b.ReturnBareChipp(nkpt)
    #     # Going to the pseudospin basis
    #     A, B = bchipp.changeBasis(ladderpp1, basis1="OSOS", basis2="OOPP")
    #     gammapp0[A, :] = gammapp0[B, :]
    #     gammapp0[:, A] = gammapp0[:, B]
    #     ladderpp1[:, :, A, :] = ladderpp1[:, :, B, :]
    #     ladderpp1[:, :, :, A] = ladderpp1[:, :, :, B]
    #     ladderpp2[:, :, A, :] = ladderpp2[:, :, B, :]
    #     ladderpp2[:, :, :, A] = ladderpp2[:, :, :, B]
    #     ladderph[:, :, A, :] = ladderph[:, :, B, :]
    #     ladderph[:, :, :, A] = ladderph[:, :, :, B]

    #     if verbose:
    #         # Print max for Phi_ph, Phi^1_pp, Phi^2_pp
    #         print("\nPhi_ph:")
    #         max_lad = np.zeros((4, 4))
    #         for i, j in itp(range(4), repeat=2):
    #             i1, i2 = norb**2*i, norb**2*(i+1)
    #             j1, j2 = norb**2*j, norb**2*(j+1)
    #             tmp_lad = ladderph[:, :, i1:i2, j1:j2]
    #             tmp_lad = np.amax(np.absolute(tmp_lad))
    #             max_lad[i, j] = tmp_lad
    #         print(max_lad)
    #         print("\nPhi^1_pp:")
    #         for i, j in itp(range(4), repeat=2):
    #             i1, i2 = norb**2*i, norb**2*(i+1)
    #             j1, j2 = norb**2*j, norb**2*(j+1)
    #             tmp_lad = ladderpp1[:, :, i1:i2, j1:j2]
    #             tmp_lad = np.amax(np.absolute(tmp_lad))
    #             max_lad[i, j] = tmp_lad
    #         print(max_lad)
    #         print("\nPhi^2_pp:")
    #         for i, j in itp(range(4), repeat=2):
    #             i1, i2 = norb**2*i, norb**2*(i+1)
    #             j1, j2 = norb**2*j, norb**2*(j+1)
    #             tmp_lad = ladderpp1[:, :, i1:i2, j1:j2]
    #             tmp_lad = np.amax(np.absolute(tmp_lad))
    #             max_lad[i, j] = tmp_lad
    #         print(max_lad)

    #     Gpp0, Gpp1, Gpp2 = (self.ConstructTwoFreqGammapp(freqs, nkpt,
    #                         gammapp0, ladderpp1, ladderpp2))

    #     if write_path is not None:
    #         # Write only the even-parity part in files
    #         prt_ladd1_Re = PhysObj(2*norb**2, nkpt, 2*nw, freqs=2)
    #         prt_ladd1_Im = PhysObj(2*norb**2, nkpt, 2*nw, freqs=2)
    #         prt_ladd2_Re = PhysObj(2*norb**2, nkpt, 2*nw, freqs=2)
    #         prt_ladd2_Im = PhysObj(2*norb**2, nkpt, 2*nw, freqs=2)

    #         i1, i2 = norb**2, 3*norb**2
    #         prt_ladd1_Re[:, :, :] = (.5*Gpp0[i1:i2, i1:i2].real +
    #                                  Gpp1[:, :, :, i1:i2, i1:i2].real)
    #         prt_ladd1_Im[:, :, :] = (.5*Gpp0[i1:i2, i1:i2].imag +
    #                                  Gpp1[:, :, :, i1:i2, i1:i2].imag)
    #         prt_ladd2_Re[:, :, :] = (.5*Gpp0[i1:i2, i1:i2].real +
    #                                  Gpp2[:, :, :, i1:i2, i1:i2].real)
    #         prt_ladd2_Im[:, :, :] = (.5*Gpp0[i1:i2, i1:i2].imag +
    #                                  Gpp2[:, :, :, i1:i2, i1:i2].imag)

    #         topfile = "#\n#"
    #         prt_ladd1_Re.write("%s%s.ReGammapp1_pm_mp" % (write_path,
    #                                                       self.system),
    #                            topfile)
    #         prt_ladd1_Im.write("%s%s.ImGammapp1_pm_mp" % (write_path,
    #                                                       self.system),
    #                            topfile)
    #         prt_ladd2_Re.write("%s%s.ReGammapp2_pm_mp" % (write_path,
    #                                                       self.system),
    #                            topfile)
    #         prt_ladd2_Im.write("%s%s.ImGammapp2_pm_mp" % (write_path,
    #                                                       self.system),
    #                            topfile)
    #     print("##############################")

    def PreparePseudospinEffectivePairing(self, nkpt, freqs, write_path=None,
                                          verbose=False, eo_parity=0):
        norb = 3
        nw, nv = freqs["nw"], freqs["nv"]
        # nw, nv, nw_chi = freqs["nw"], freqs["nv"], freqs["nw_chi"]

        # Gamma^0_ph and Phi_ph
        self.SetGammaph()
        ladderph = self.ReturnLadderph(nkpt)
        gammaph = self.gammaph.gamma

        # Rotate to the pp channel
        gammapp0, ladderpp1, ladderpp2 = (self.ReturnLadderpp(nkpt, nv,
                                          gammaph, ladderph))

        b = Barechi2(self.system, self.conn,
                     id=self.cells["barechi2.barechi2id"])
        bchipp = b.ReturnBareChipp(nkpt)
        nno = norb**2
        A, B = bchipp.changeBasis(ladderpp1, basis1="OSOS", basis2="OOPP")
        gammapp0[A, :] = gammapp0[B, :]
        gammapp0[:, A] = gammapp0[:, B]
        ladderpp1[:, :, A, :] = ladderpp1[:, :, B, :]
        ladderpp1[:, :, :, A] = ladderpp1[:, :, :, B]
        ladderpp2[:, :, A, :] = ladderpp2[:, :, B, :]
        ladderpp2[:, :, :, A] = ladderpp2[:, :, :, B]
        ladderph[:, :, A, :] = ladderph[:, :, B, :]
        ladderph[:, :, :, A] = ladderph[:, :, :, B]

        if verbose:
            # Print max for Phi_ph, Phi^1_pp, Phi^2_pp
            print("\nPhi_ph:")
            max_lad = np.zeros((4, 4))
            for i, j in itp(range(4), repeat=2):
                i1, i2 = norb**2*i, norb**2*(i+1)
                j1, j2 = norb**2*j, norb**2*(j+1)
                tmp_lad = ladderph[:, :, i1:i2, j1:j2]
                tmp_lad = np.amax(np.absolute(tmp_lad))
                max_lad[i, j] = tmp_lad
            print(max_lad)
            print("\nPhi^1_pp:")
            for i, j in itp(range(4), repeat=2):
                i1, i2 = norb**2*i, norb**2*(i+1)
                j1, j2 = norb**2*j, norb**2*(j+1)
                tmp_lad = ladderpp1[:, :, i1:i2, j1:j2]
                tmp_lad = np.amax(np.absolute(tmp_lad))
                max_lad[i, j] = tmp_lad
            print(max_lad)
            print("\nPhi^2_pp:")
            for i, j in itp(range(4), repeat=2):
                i1, i2 = norb**2*i, norb**2*(i+1)
                j1, j2 = norb**2*j, norb**2*(j+1)
                tmp_lad = ladderpp1[:, :, i1:i2, j1:j2]
                tmp_lad = np.amax(np.absolute(tmp_lad))
                max_lad[i, j] = tmp_lad
            print(max_lad)

        Gpp0, Gpp1, Gpp2 = (self.ConstructTwoFreqGammapp(freqs, nkpt,
                            gammapp0, ladderpp1, ladderpp2))

        effG0, effG1, effG2 = self.EffectiveGammapp(Gpp0, Gpp1, Gpp2, nw)
        eff_tmp = self.EffectiveGammapp2(Gpp0, Gpp1, Gpp2, nw)
        effG0_1, effG0_2 = eff_tmp[0], eff_tmp[1]
        effG1_1, effG1_2 = eff_tmp[2], eff_tmp[3]
        # effG2_1, effG2_2 = eff_tmp[4], eff_tmp[5]

        def amax(A, rep):
            for _i in range(rep):
                A = np.amax(A, axis=0)
            return A

        if verbose:
            print("\n")
            print("##############################")
            print("# Effective Ladder functions #")
            print("##############################")
            print(np.round(np.absolute(effG0), 5))
            print(np.amax(np.absolute(effG0)))
            max_eff = np.zeros((2, 2))
            for p1, p2 in itp(range(2), repeat=2):
                eG = effG0[p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 2)
            print(max_eff)

            print(np.round(np.amax(np.amax(np.amax(np.absolute(effG1), axis=0),
                                           axis=0), axis=0), 5))
            print(np.amax(np.absolute(effG1)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = effG1[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

            print(np.round(np.amax(np.amax(np.amax(np.absolute(effG2), axis=0),
                                           axis=0), axis=0), 5))
            print(np.amax(np.absolute(effG2)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = effG2[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

        if write_path is not None:
            print("nw:", nw)
            # Write only the even-parity part in files
            topfile = "#\n#"
            prt_ladd1_Re = PhysObj(norb**2, nkpt, 2*nw, freqs=2)
            prt_ladd1_Im = PhysObj(norb**2, nkpt, 2*nw, freqs=2)
            prt_ladd2_Re = PhysObj(norb**2, nkpt, 2*nw, freqs=2)
            prt_ladd2_Im = PhysObj(norb**2, nkpt, 2*nw, freqs=2)

            i1, i2 = (eo_parity)*norb**2, (eo_parity+1)*norb**2
            print("Shapes:", prt_ladd1_Re[:, :, :].shape,
                  effG1[:, :, :, i1:i2, i1:i2].shape)

            prt_ladd1_Re[:, :, :] = (.5*effG0[i1:i2, i1:i2].real +
                                     effG1[:, :, :, i1:i2, i1:i2].real)
            prt_ladd1_Im[:, :, :] = (.5*effG0[i1:i2, i1:i2].imag +
                                     effG1[:, :, :, i1:i2, i1:i2].imag)
            prt_ladd2_Re[:, :, :] = (.5*effG0[i1:i2, i1:i2].real +
                                     effG2[:, :, :, i1:i2, i1:i2].real)
            prt_ladd2_Im[:, :, :] = (.5*effG0[i1:i2, i1:i2].imag +
                                     effG2[:, :, :, i1:i2, i1:i2].imag)

            prt_ladd1_Re.write("%s%s.ReGammapp1_pm" % (write_path,
                                                       self.system),
                               topfile, freq_shift=nw)
            prt_ladd1_Im.write("%s%s.ImGammapp1_pm" % (write_path,
                                                       self.system),
                               topfile, freq_shift=nw)
            prt_ladd2_Re.write("%s%s.ReGammapp2_pm" % (write_path,
                                                       self.system),
                               topfile, freq_shift=nw)
            prt_ladd2_Im.write("%s%s.ImGammapp2_pm" % (write_path,
                                                       self.system),
                               topfile, freq_shift=nw)
        print("##############################")

        if verbose:
            print("\n")
            print("########################################")
            print("# Effective Ladder functions FREQ DIAG #")
            print("########################################")

            nno = norb**2
            bchipp[:] = np.around(bchipp[:], 5)
            print("Barechi pp in OSOS basis")
            print("k, w = 0, 0")
            print(bchipp[0, 0])
            # for i, j in itp(range(4), repeat=2):
            #     if i != j:
            #         a = bchipp[:, :, i*nno:(i+1)*nno, j*nno:(j+1)*nno]
            #         a = 0*a

            A, B = bchipp.changeBasis(ladderpp1, basis1="OSOS", basis2="OOSS")
            bchipp.Re[:, :, A, :] = bchipp.Re[:, :, B, :]
            bchipp.Re[:, :, :, A] = bchipp.Re[:, :, :, B]
            bchipp.Im[:, :, A, :] = bchipp.Im[:, :, B, :]
            bchipp.Im[:, :, :, A] = bchipp.Im[:, :, :, B]
            print("From OSOS to OOSS:", A, B)
            print("Barechi pp in OOSS basis")
            print("k, w = max, 0; Re/Im")
            print(np.amax(np.absolute(bchipp[:, 0].real), axis=0))
            print(np.amax(np.absolute(bchipp[:, 0].imag), axis=0))

            A, B = bchipp.changeBasis(ladderpp1, basis1="OOSS", basis2="OSOS")
            bchipp.Re[:, :, A, :] = bchipp.Re[:, :, B, :]
            bchipp.Re[:, :, :, A] = bchipp.Re[:, :, :, B]
            bchipp.Im[:, :, A, :] = bchipp.Im[:, :, B, :]
            bchipp.Im[:, :, :, A] = bchipp.Im[:, :, :, B]
            print("From OOSS to OSOS:", A, B)
            print("Barechi pp in OSOS basis")
            print("k, w = , 0")
            print(np.amax(np.absolute(bchipp[:, 0].real), axis=0))
            print(np.amax(np.absolute(bchipp[:, 0].imag), axis=0))

            A, B = bchipp.changeBasis(ladderpp1, basis1="OSOS", basis2="OOPP")
            bchipp.Re[:, :, A, :] = bchipp.Re[:, :, B, :]
            bchipp.Re[:, :, :, A] = bchipp.Re[:, :, :, B]
            bchipp.Im[:, :, A, :] = bchipp.Im[:, :, B, :]
            bchipp.Im[:, :, :, A] = bchipp.Im[:, :, :, B]

            print("From OSOS to OOPP:", A, B)
            print("Barechi pp in OPP basis")
            print("shape bchipp: ", bchipp.Re.Obj.shape)
            print("k, w = , 0")
            print(np.amax(np.absolute(bchipp[:, 0].real), axis=0))
            print(np.amax(np.absolute(bchipp[:, 0].imag), axis=0))

            chi_bK = np.zeros((len(bchipp), 2*nw, 2*nno, 2*nno),
                              dtype=complex)
            chi_bK2 = np.zeros((len(bchipp), 2*nw, 2*nno, 2*nno),
                               dtype=complex)
            rw = range(nw, 2*nw)
            rw2 = range(nw-1, -1, -1)
            chi_bK[:, rw, :nno, :nno] = bchipp[:, range(nw),
                                               nno:2*nno, nno:2*nno]
            chi_bK[:, rw, nno:, nno:] = bchipp[:, range(nw),
                                               nno:2*nno, nno:2*nno]
            chi_bK2[:, rw2, :nno, :nno] = bchipp[:, range(nw),
                                                 2*nno:3*nno, 2*nno:3*nno]
            chi_bK2[:, rw2, nno:, nno:] = bchipp[:, range(nw),
                                                 2*nno:3*nno, 2*nno:3*nno]

            # chi_bK[:, rw, :nno, :nno] = bchipp[:, range(nw),
            #                                    :nno, :nno]
            # chi_bK[:, rw, nno:, nno:] = bchipp[:, range(nw),
            #                                    :nno, :nno]
            # chi_bK2[:, rw2, :nno, :nno] = bchipp[:, range(nw),
            #                                      3*nno:, 3*nno:]
            # chi_bK2[:, rw2, nno:, nno:] = bchipp[:, range(nw),
            #                                      3*nno:, 3*nno:]
            print("k, w = , w0")
            print(np.amax(chi_bK[:, nw].real, axis=0))
            print(np.amax(chi_bK[:, nw].imag, axis=0))

            # Tchi_bK = np.copy(chi_bK2)
            A, B = [], []
            for o1, o2 in itp(range(norb), repeat=2):
                A.append(o1*norb+o2)
                B.append(o2*norb+o1)
            for o1, o2 in itp(range(norb), repeat=2):
                A.append(o1*norb+o2+norb**2)
                B.append(o2*norb+o1+norb**2)
            print("A: ", A)
            print("B: ", B)
            chi_bK2[:, :, A, :] = chi_bK2[:, :, B, :]
            chi_bK2[:, :, :, A] = chi_bK2[:, :, :, B]

            # Tchi_bK[:, :, A, :] = Tchi_bK[:, :, B, :]
            # Tchi_bK[:, :, :, A] = Tchi_bK[:, :, :, B]
            # Tchi_bK = np.conj(chi_bK.transpose(0, 1, 3, 2))
            # Tchi_bK2 = np.conj(chi_bK2.transpose(0, 1, 3, 2))
            # Tchi_bK = chi_bK.transpose(0, 1, 3, 2)
            # Tchi_bK = np.conj(chi_bK)
            # Tchi_bK = np.copy(chi_bK)
            # Tchi_bK2 = np.copy(chi_bK2)

            for w in range(1, nw+1):
                print(nw-w, nw+w-1)
                tmp_chi = np.conj(chi_bK.transpose(0, 1, 3, 2)[:, nw+w-1])
                tmp_chi2 = np.conj(chi_bK2.transpose(0, 1, 3, 2)[:, nw-w])
                # chi_bK[:, nw-w] = np.conj(tmp_chi)
                # chi_bK2[:, nw+w-1] = np.conj(tmp_chi2)
                for k in range(len(bchipp)):
                    chi_bK[k, nw-w] = tmp_chi[k]
                    chi_bK2[k, nw+w-1] = tmp_chi2[k]
                    # chi_bK[k, nw-w] = (np.transpose(chi_bK[k, nw+w-1]))
                    # chi_bK2[k, nw+w-1] = (np.transpose(chi_bK2[k, nw-w]))
            print("k, w = 0, -w0")
            print(chi_bK[0, nw])
            print(chi_bK[0, nw-1])
            print(chi_bK2[0, nw-1])

            eff2G0 = np.zeros(effG1.shape, dtype=complex)
            eff2G1 = np.zeros(effG1.shape, dtype=complex)
            eff2G2 = np.zeros(effG1.shape, dtype=complex)

            minus_w = [x for x in np.arange(2*nw-1, -1, -1)]

            eG0_11 = np.zeros(eff2G1.shape, dtype=complex)
            eG0_12 = np.zeros(eff2G1.shape, dtype=complex)
            eG0_21 = np.zeros(eff2G1.shape, dtype=complex)
            eG0_22 = np.zeros(eff2G1.shape, dtype=complex)

            eG1_11 = np.zeros(eff2G1.shape, dtype=complex)
            eG1_12 = np.zeros(eff2G1.shape, dtype=complex)
            eG1_21 = np.zeros(eff2G1.shape, dtype=complex)
            eG1_22 = np.zeros(eff2G1.shape, dtype=complex)

            eG2_11 = np.zeros(eff2G1.shape, dtype=complex)
            eG2_12 = np.zeros(eff2G1.shape, dtype=complex)
            eG2_21 = np.zeros(eff2G1.shape, dtype=complex)
            eG2_22 = np.zeros(eff2G1.shape, dtype=complex)

            print("nkpt: ", len(bchipp))
            for k in range(len(bchipp)):
                for w in range(2*nw):
                    for w2 in range(2*nw):
                        mw = minus_w
                        # eG1_11[k, w, w2] = (effG1[k, w, w2] @ chi_bK[k, w2])
                        # eG1_12[k, w, w2] = (effG1[k, w][mw][w2] @
                        #                     chi_bK[k][mw][w2])
                        # eG1_21[k, w, w2] = (effG1[k][mw][w][w2] @
                        #                     chi_bK[k, w2])
                        # eG1_22[k, w, w2] = (effG1[k][mw][w][mw][w2] @
                        #                     chi_bK[k][mw][w2])

                        # eG2_11[k, w, w2] = (effG2[k, w, w2] @ chi_bK[k, w2])
                        # eG2_12[k, w, w2] = (effG2[k, w][mw][w2] @
                        #                     chi_bK[k][mw][w2])
                        # eG2_21[k, w, w2] = (effG2[k][mw][w][w2] @
                        #                     chi_bK[k, w2])
                        # eG2_22[k, w, w2] = (effG2[k][mw][w][mw][w2] @
                        #                     chi_bK[k][mw][w2])

                        eG1_11[k, w, w2] = (effG1_1[k, w, w2] @
                                            chi_bK[k, w2] +
                                            effG1_2[k, w, w2] @
                                            chi_bK2[k, w2])
                        eG1_12[k, w, w2] = (effG1_1[k, w][mw][w2] @
                                            chi_bK[k][mw][w2] +
                                            effG1_2[k, w][mw][w2] @
                                            chi_bK2[k][mw][w2])
                        eG1_21[k, w, w2] = (effG1_1[k][mw][w][w2] @
                                            chi_bK[k, w2] +
                                            effG1_2[k][mw][w][w2] @
                                            chi_bK2[k, w2])
                        eG1_22[k, w, w2] = (effG1_1[k][mw][w][mw][w2] @
                                            chi_bK[k][mw][w2] +
                                            effG1_2[k][mw][w][mw][w2] @
                                            chi_bK2[k][mw][w2])

                        eG0_11[k, w, w2] = (effG0_1 @
                                            chi_bK[k, w2] +
                                            effG0_2 @
                                            chi_bK2[k, w2])
                        eG0_12[k, w, w2] = (effG0_1 @
                                            chi_bK[k][mw][w2] +
                                            effG0_2 @
                                            chi_bK2[k][mw][w2])
                        eG0_21[k, w, w2] = (effG0_1 @
                                            chi_bK[k, w2] +
                                            effG0_2 @
                                            chi_bK2[k, w2])
                        eG0_22[k, w, w2] = (effG0_1 @
                                            chi_bK[k][mw][w2] +
                                            effG0_2 @
                                            chi_bK2[k][mw][w2])
            print("V_KK' + V_KbK' + V_bKK' + V_bKbK'")
            eff2G0 = (eG0_11 + eG0_12 + eG0_21 + eG0_22)
            eff2G0 = np.round(eff2G0, 5)
            eff2G1 = (eG1_11 + eG1_12 + eG1_21 + eG1_22)
            eff2G1 = np.round(eff2G1, 5)
            eff2G2 = (eG2_11 + eG2_12 + eG2_21 + eG2_22)
            eff2G2 = np.round(eff2G2, 5)
            eff2G = eff2G1  # - eff2G2
            # eff2G1 = (effG1[:] + effG1[:, :, minus_w] -
            #           effG1[:, minus_w] - effG1[:, minus_w][:, :, minus_w])
            max_eff = np.zeros((2, 2))
            print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G0),
                                                   axis=0),
                                           axis=0), axis=0), 5))
            print(np.amax(np.absolute(eff2G1)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = eff2G[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

            print("V_KK' - V_KbK' + V_bKK' - V_bKbK'")
            eff2G0 = (eG0_11 - eG0_12 + eG0_21 - eG0_22)
            eff2G0 = np.round(eff2G0, 5)
            eff2G1 = (eG1_11 - eG1_12 + eG1_21 - eG1_22)
            eff2G1 = np.round(eff2G1, 5)
            eff2G2 = (eG2_11 - eG2_12 + eG2_21 - eG2_22)
            eff2G2 = np.round(eff2G2, 5)
            eff2G = eff2G1  # eff2G1 - eff2G2
            # mw = minus_w
            # eff2G1 = (effG1[:] + effG1[:, :, mw] -
            #           effG1[:, mw] - effG1[:, mw][:, :, mw])
            max_eff = np.zeros((2, 2))

            print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G0),
                                                   axis=0),
                                           axis=0), axis=0), 5))
            print(np.amax(np.absolute(eff2G1)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = eff2G[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

            print("V_KK' + V_KbK' - V_bKK' - V_bKbK'")
            eff2G0 = (eG0_11 + eG0_12 - eG0_21 - eG0_22)
            eff2G0 = np.round(eff2G0, 5)
            eff2G1 = (eG1_11 + eG1_12 - eG1_21 - eG1_22)
            eff2G1 = np.round(eff2G1, 5)
            eff2G2 = (eG2_11 + eG2_12 - eG2_21 - eG2_22)
            eff2G2 = np.round(eff2G2, 5)
            eff2G = eff2G1  # eff2G1 - eff2G2
            # mw = minus_w
            # eff2G1 = (effG1[:] + effG1[:, :, mw] -
            #           effG1[:, mw, w] - effG1[:, mw][:, :, mw])
            max_eff = np.zeros((2, 2))

            # print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G1),
            #                                        axis=0),
            #                                axis=0), axis=0), 5))
            # print(np.amax(np.absolute(eff2G1)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = eff2G[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

            print("V_KK' - V_KbK' - V_bKK' + V_bKbK'")
            eff2G0 = (eG0_11 - eG0_12 - eG0_21 + eG0_22)
            eff2G0 = np.round(eff2G0, 5)
            eff2G1 = (eG1_11 - eG1_12 - eG1_21 + eG1_22)
            eff2G1 = np.round(eff2G1, 5)
            eff2G2 = (eG2_11 - eG2_12 - eG2_21 + eG2_22)
            eff2G2 = np.round(eff2G2, 5)
            eff2G = eff2G1  # eff2G1 - eff2G2
            # mw = minus_w
            # eff2G1 = (effG1[:] - effG1[:, :, mw] -
            #           effG1[:, mw] + effG1[:, mw][:, :, mw])
            # print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G1),
            #                                        axis=0),
            #                                axis=0), axis=0), 5))
            # print(np.amax(np.absolute(eff2G1)))
            for p1, p2 in itp(range(2), repeat=2):
                eG = eff2G[:, :, :, p1*norb**2:(p1+1)*norb**2,
                           p2*norb**2:(p2+1)*norb**2]
                max_eff[p1, p2] = amax(np.absolute(eG), 5)
            print(max_eff)

            # eff2G2 = (effG2[:] - effG2[:, :, minus_w] -
            #           effG2[:, minus_w] + effG2[:, minus_w][:, :, minus_w])
            # print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G2),
            #                                        axis=0),
            #                                axis=0), axis=0), 5))
            # print(np.amax(np.absolute(eff2G2)))
            # for p1, p2 in itp(range(2), repeat=2):
            #     eG = eff2G2[:, :, :, p1*norb**2:(p1+1)*norb**2,
            #                 p2*norb**2:(p2+1)*norb**2]

            # eff2G2 = (effG2[:] - effG2[:, :, minus_w] -
            #           effG2[:, minus_w] + effG2[:, minus_w][:, :, minus_w])
            # print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G2),
            #                                        axis=0),
            #                                axis=0), axis=0), 5))
            # print(np.amax(np.absolute(eff2G2)))
            # for p1, p2 in itp(range(2), repeat=2):
            #     eG = eff2G2[:, :, :, p1*norb**2:(p1+1)*norb**2,
            #                 p2*norb**2:(p2+1)*norb**2]
            # eff2G2 = (effG2[:] + effG2[:, :, minus_w] -
            #           effG2[:, minus_w] - effG2[:, minus_w][:, :, minus_w])
            # print(np.round(np.amax(np.amax(np.amax(np.absolute(eff2G2),
            #                                        axis=0),
            #                                axis=0), axis=0), 5))
            # print(np.amax(np.absolute(eff2G2)))
            # for p1, p2 in itp(range(2), repeat=2):
            #     eG = eff2G2[:, :, :, p1*norb**2:(p1+1)*norb**2,
            #                 p2*norb**2:(p2+1)*norb**2]
            #     max_eff[p1, p2] = amax(np.absolute(eG), 5)
            # print(max_eff)

            #     max_eff[p1, p2] = amax(np.absolute(eG), 5)
            # print(max_eff)

    def PlotDressedSusPath(self, plotParams=None, add_obj=None):
        if add_obj is not None:
            print(add_obj.shape)
        if plotParams is None:
            plotParams = {}

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""
        if len(self.cells) == 0:
            self.cells = self.Load()

        # Find parameters constructing the object
        nqpath = self.cells["barechi2.nqpath"]
        nfreqs = self.cells["barechi2.nivn"]

        U = self.cells["dressed.dressedU"]
        Up = self.cells["dressed.dressedUp"]
        Upp = self.cells["dressed.dressedUpp"]
        J = self.cells["dressed.dressedJ"]
        Jp = self.cells["dressed.dressedJp"]

        default = PlotParams()
        default.params["indices"] = [[[[0, 0], [4, 4], [8, 8]],
                                      [[1, 1], [2, 2], [5, 5]]]]
        default.params["plot_colors"] = [[["blue", "red", "green"],
                                          ["black", "orange", "purple"]]]
        default.params["plot_labels"] = [[["xy;xy", "yz;yz", "zx;zx"],
                                          ["xy;yz", "xy;zx", "yz;zx"]]]
        default.params["labels"] = ["xy", "yz", "zx"]
        xpath = ([[0, (nqpath-1)/3-1, (nqpath-1)*2/3-1, nqpath-1],
                  [r"$\Gamma$", "M", "X", r"$\Gamma$"]])
        default.params["xticks"] = [[xpath, xpath]]
        default.params["title"] = ("Dressed Susceptibilities in particle-hole "
                                   "(ph) and particle-particle (pp) channels\n"
                                   "U = %f; J = %f; Up = %f; Upp = %f"
                                   "Jp = %f" %
                                   (U, J, Up, Upp, Jp))
        default.params["freqs"] = range(nfreqs)
        default.params["tight_layout"] = False
        default.params["show"] = False
        default.params["legend_loc"] = "best"
        plotParams.setDefaults(default)

        for v in plotParams["freqs"]:
            fig, ax = plotting.setFigure(plotParams)

            # Construct physical object
            obj = self.ReturnDressedSusph(nqpath).Re
            print(obj.shape)
            if add_obj is not None:
                print(add_obj.shape)

            plotting.plotPath(ax, 0, 0, obj, v, plotParams)
            plotting.plotPath(ax, 0, 1, obj, v, plotParams)

            if add_obj is not None:
                plotting.plotPath(ax, 0, 0, add_obj, 0, plotParams)
                plotting.plotPath(ax, 0, 1, add_obj, 0, plotParams)

            plotting.setSubplot(ax, plotParams,
                                printVariables={"v": str(v)})

            for a in [0, 1]:
                xticks = plotParams["xticks"][0][0][0]
                for i in range(1, len(xticks)-1):
                    plotting.plotVerticalLine(ax[a], xticks[i],
                                              plotParams)

            saveplace = (self.conn.groundDir + self.baseDir +
                         "DressedSus_v%d.png" % v)
            plotting.finish(plotParams, saveplace)

    def PlotLadderPath(self, plotParams=None):
        if plotParams is None:
            plotParams = {}

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""

        nkpt = self.cells["barechi2.nqpath"]
        nqpath = self.cells["barechi2.nqpath"]

        if "indices" in plotParams:
            _indices = plotParams["indices"]
        else:
            raise ValueError("ERROR: Too many possibilities for indices, "
                             "please specify in plotParams[\"indices\"].")

        if "plot_colors" in plotParams:
            _plot_colors = plotParams["plot_colors"]
        else:
            _plot_colors = ([["blue", "red", "green"],
                            ["black", "orange", "purple"]])
        assert(len(_indices) == len(_plot_colors))

        if "figsize" in plotParams:
            _figsize = plotParams["figsize"]
        else:
            _figsize = (6, 5)

        if "titlesize" in plotParams:
            _titlesize = plotParams["titlesize"]
        else:
            _titlesize = 10

        if "labels" in plotParams:
            _labels = plotParams["labels"]
        else:
            _labels = ["z", "x", "y"]

        if "xticks" in plotParams:
            _xticks = plotParams["xticks"]
        else:
            _xticks = ([[0, nqpath / 3 - 1, nqpath * 2 / 3 - 1, nqpath - 1],
                       [r"$\Gamma$", "M", "X", r"$\Gamma$"]])

        if "freqs" in plotParams:
            _freqs = plotParams["freqs"]
        else:
            _freqs = range(self.cells["dressed.mivn"] + 1)

        if "complexParts" in plotParams:
            _complexParts = plotParams["complexParts"]
        else:
            _complexParts = ["Re", "Im"]

        if "show" in plotParams:
            _show = plotParams["show"]
        else:
            _show = False

        dU = self.cells["dressed.dressedU"]
        dJ = self.cells["dressed.dressedJ"]
        ndim = self.cells["barechi2.norb"]
        G = Gamma(ndim, dU, dJ)
        G.constructRPA()

        title = ("Ladder functions in density (d) and magnetic (m) channels\n"
                 "U = %f; J = %f" % (dU, dJ))

        for c in _complexParts:
            if c not in ["Re", "Im"]:
                raise ValueError("%s in plotParams[\"complexParts\"] "
                                 "is not recognised." % c)
            Den = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Den.Read("%sOutput/%s.%sDressedSusphDensity" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)
            Mag = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Mag.Read("%sOutput/%s.%sDressedSusphMagnetic" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)

            print(G[0] * Den[0][0] * G[0])

            for v in _freqs:
                fig, ax = plt.subplots(2, len(_indices), figsize=_figsize)
                Ladder_d = np.zeros((nkpt, ndim**2, ndim**2))
                Ladder_m = np.zeros((nkpt, ndim**2, ndim**2))

                ylabel = ([r"$[\Phi_{%s}(i\nu_{%d}, \vec{q})]_"
                          "{l_1,l_2;l_3,l_4}$" % (t, v)
                           for t in ["d", "m"]])

                for k in range(nkpt):
                    Ladder_d[k] = G[0].dot(Den[k][v].dot(G[0]))
                    Ladder_m[k] = G[1].dot(Mag[k][v].dot(G[1]))

                for j, ll in enumerate(_indices):
                    for i, l in enumerate(ll):
                        ax[0, j].plot(np.arange(nqpath),
                                      Ladder_d[:, l[0], l[1]],
                                      label="%s%s;%s%s" %
                                      (_labels[int(l[0] / ndim)],
                                      _labels[int(l[0] % ndim)],
                                      _labels[int(l[1] / ndim)],
                                      _labels[int(l[1] % ndim)]),
                                      marker='', linestyle='-', linewidth=3,
                                      color=_plot_colors[j][i])
                        ax[1, j].plot(np.arange(nqpath),
                                      Ladder_m[:, l[0], l[1]],
                                      label="%s%s;%s%s" %
                                      (_labels[int(l[0] / ndim)],
                                      _labels[int(l[0] % ndim)],
                                      _labels[int(l[1] / ndim)],
                                      _labels[int(l[1] % ndim)]),
                                      marker='', linestyle='-', linewidth=3,
                                      color=_plot_colors[j][i])

                    ax[0, j].set_xlim([0, nqpath-1])
                    ax[1, j].set_xlim([0, nqpath-1])

                    plt.sca(ax[0, j])
                    plt.sca(ax[1, j])

                    ax[0, j].set_xticks(_xticks[0], _xticks[1])
                    ax[0, j].set_xticks(_xticks[0], _xticks[1])

                    ymin0, ymax0 = ax[0, j].get_ylim()
                    ymin1, ymax1 = ax[1, j].get_ylim()
                    for t in range(1, len(_xticks[0]) - 1):
                        ax[0, j].plot([_xticks[0][t], _xticks[0][t]],
                                      [ymin0, ymax0],
                                      color="black", linestyle='--')
                        ax[1, j].plot([_xticks[0][t], _xticks[0][t]],
                                      [ymin1, ymax1],
                                      color="black", linestyle='--')
                    format = FormatStrFormatter("%.1f")
                    ax[0, j].yaxis.set_major_formatter(format)
                    ax[1, j].yaxis.set_major_formatter(format)

                    if j == 0:
                        ax[0, j].set_ylabel(ylabel[0], fontsize=16)
                        ax[1, j].set_ylabel(ylabel[1], fontsize=16)

                    ax[0, j].legend(loc="best")
                    ax[1, j].legend(loc="best")

                plt.suptitle(title, fontsize=_titlesize)
                plt.savefig(self.conn.groundDir + self.baseDir +
                            "%sLadders_v%d.png" % (c, v))
                if _show:
                    plt.show()
                else:
                    plt.draw()

    def PlotVertexPath(self, plotParams=None):
        if plotParams is None:
            plotParams = {}

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""

        nkpt = self.cells["barechi2.nqpath"]
        nqpath = self.cells["barechi2.nqpath"]

        if "indices" in plotParams:
            _indices = plotParams["indices"]
        else:
            raise ValueError("ERROR: Too many possibilities for indices, "
                             "please specify in plotParams[\"indices\"].")

        if "plot_colors" in plotParams:
            _plot_colors = plotParams["plot_colors"]
        else:
            _plot_colors = ([["blue", "red", "green"],
                            ["black", "orange", "purple"]])
        assert(len(_indices) == len(_plot_colors))

        if "figsize" in plotParams:
            _figsize = plotParams["figsize"]
        else:
            _figsize = (6, 5)

        if "titlesize" in plotParams:
            _titlesize = plotParams["titlesize"]
        else:
            _titlesize = 10

        if "labels" in plotParams:
            _labels = plotParams["labels"]
        else:
            _labels = ["z", "x", "y"]

        if "xticks" in plotParams:
            _xticks = plotParams["xticks"]
        else:
            _xticks = [[0, nqpath / 3 - 1, nqpath * 2 / 3 - 1, nqpath - 1],
                       [r"$\Gamma$", "M", "X", r"$\Gamma$"]]

        if "freqs" in plotParams:
            _freqs = plotParams["freqs"]
        else:
            _freqs = range(self.cells["dressed.mivn"] + 1)

        if "complexParts" in plotParams:
            _complexParts = plotParams["complexParts"]
        else:
            _complexParts = ["Re", "Im"]

        if "show" in plotParams:
            _show = plotParams["show"]
        else:
            _show = False

        dU = self.cells["dressed.dressedU"]
        dJ = self.cells["dressed.dressedJ"]
        ndim = self.cells["barechi2.norb"]
        G = Gamma(ndim, dU, dJ)
        G.constructRPA()

        for c in _complexParts:
            if c not in ["Re", "Im"]:
                raise ValueError("%s in plotParams[\"complexParts\"] "
                                 "is not recognised." % c)
            Den = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Den.Read("%sOutput/%s.%sDressedSusphDensity" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)
            Mag = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Mag.Read("%sOutput/%s.%sDressedSusphMagnetic" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)

            print(G[0] * Den[0][0] * G[0])

            for v in _freqs:
                for chan in ["s", "t"]:
                    if chan == "s":
                        channel = "singlet"
                    elif chan == "t":
                        channel = "triplet"
                    title = ("Pairing vertex components in %s channel\n"
                             "U = %f; J = %f" % (channel, dU, dJ))

                    fig, ax = plt.subplots(2, len(_indices), figsize=_figsize)
                    Ladder_d = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_m = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_plus = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_minus = np.zeros((nkpt, ndim**2, ndim**2))

                    ylabel = ([r"$[\Phi^{%s}(i\nu_{%d}, \vec{q})]_"
                              "{l_1,l_2;l_3,l_4}$" % (sign, v)
                               for sign in ["-", "+"]])

                    for k in range(nkpt):
                        Ladder_d[k] = G[0].dot(Den[k][v].dot(G[0]))
                        Ladder_m[k] = G[1].dot(Mag[k][v].dot(G[1]))

                        ind = itertools.product(range(ndim), range(ndim),
                                                range(ndim), range(ndim))
                        for l1, l2, l3, l4 in ind:
                            ll1 = l1*ndim+l2
                            ll2 = l3*ndim+l4

                            ll3 = l2*ndim+l4
                            ll4 = l3*ndim+l1
                            ll5 = l1*ndim+l4
                            ll6 = l3*ndim+l2

                            a1 = Ladder_d[k][ll3, ll4]
                            a2 = Ladder_m[k][ll3, ll4]

                            b1 = Ladder_d[k][ll5, ll6]
                            b2 = Ladder_m[k][ll5, ll6]

                            if chan == "s":
                                Ladder_minus[k][ll1, ll2] = .5*a1 - 1.5*a2
                                Ladder_plus[k][ll1, ll2] = .5*b1 - 1.5*b2

                            if chan == "t":
                                Ladder_minus[k][ll1, ll2] = .5*a1 + .5*a2
                                Ladder_plus[k][ll1, ll2] = -.5*b1 - .5*b2

                    for j, ll in enumerate(_indices):
                        for i, l in enumerate(ll):
                            ax[0, j].plot(np.arange(nqpath),
                                          Ladder_minus[:, l[0], l[1]],
                                          label="%s%s;%s%s" %
                                          (_labels[int(l[0] / ndim)],
                                          _labels[int(l[0] % ndim)],
                                          _labels[int(l[1] / ndim)],
                                          _labels[int(l[1] % ndim)]),
                                          marker='', linestyle='-',
                                          linewidth=3,
                                          color=_plot_colors[j][i])
                            ax[1, j].plot(np.arange(nqpath),
                                          Ladder_plus[:, l[0], l[1]],
                                          label="%s%s;%s%s" %
                                          (_labels[int(l[0] / ndim)],
                                          _labels[int(l[0] % ndim)],
                                          _labels[int(l[1] / ndim)],
                                          _labels[int(l[1] % ndim)]),
                                          marker='', linestyle='-',
                                          linewidth=3,
                                          color=_plot_colors[j][i])

                        ax[0, j].set_xlim([0, nqpath-1])
                        ax[1, j].set_xlim([0, nqpath-1])

                        plt.sca(ax[0, j])
                        plt.sca(ax[1, j])

                        ax[0, j].set_xticks(_xticks[0], _xticks[1])
                        ax[0, j].set_xticks(_xticks[0], _xticks[1])

                        ymin0, ymax0 = ax[0, j].get_ylim()
                        ymin1, ymax1 = ax[1, j].get_ylim()
                        for t in range(1, len(_xticks[0]) - 1):
                            ax[0, j].plot([_xticks[0][t], _xticks[0][t]],
                                          [ymin0, ymax0],
                                          color="black", linestyle='--')
                            ax[1, j].plot([_xticks[0][t], _xticks[0][t]],
                                          [ymin1, ymax1],
                                          color="black", linestyle='--')
                        format = FormatStrFormatter("%.1f")
                        ax[0, j].yaxis.set_major_formatter(format)
                        ax[1, j].yaxis.set_major_formatter(format)

                        if j == 0:
                            ax[0, j].set_ylabel(ylabel[0], fontsize=16)
                            ax[1, j].set_ylabel(ylabel[1], fontsize=16)

                        ax[0, j].legend(loc="best")
                        ax[1, j].legend(loc="best")

                    plt.suptitle(title, fontsize=_titlesize)
                    plt.savefig(self.conn.groundDir + self.baseDir +
                                "%sPairing_chan%s_v%d.png" % (c, chan, v))
                    if _show:
                        plt.show()
                    else:
                        plt.draw()
