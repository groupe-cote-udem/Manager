import logging
import numpy as np
import matplotlib.pyplot as plt
from .projectors import Projectors
from .bases import PostProdBase


def plot_multiple_self_energy(self_array, labels=None):
    """Plot the real and imaginary part of the self energy for multiple
    systems.

    Parameters
    ----------
    self_array : list
                 The list of SelfE objects.
    labels : list, optional
             If not None, must be a list which is the label for each SelfE.
    """
    fig = plt.figure()
    ax_re = fig.add_subplot(211)
    ax_im = fig.add_subplot(212)
    if labels is None:
        labels = [""] * len(self_array)
    for self, label in zip(self_array, labels):
        x = self.freqs
        ys = self.self_energy
        label = label + "orb #" if not len(label) else label + " orb #"
        for i, self_orb in enumerate(ys):
            ax_re.plot(x, np.real(self_orb), label=(label + str(i + 1)))
            ax_im.plot(x, np.imag(self_orb))
    ax_im.set_xlabel(r"Frequency $\omega$")
    ax_re.set_ylabel(r"$\Re\left[\Sigma(\omega)\right]$")
    ax_im.set_ylabel(r"$\Im\left[\Sigma(\omega)\right]$")
    ax_re.set_title("Self energy")
    ax_re.legend(loc="best")
    plt.show()


class SelfE(PostProdBase):
    _loggername = "postprod.self_energy"

    def __init__(self, frequencies, self_energy, **kwargs):
        """Object describing the Self Energy.

        Parameters
        ----------
        frequencies : array
                      The frequencies where the self energy is defined.
        self_energy : complex array
                      The self energy (the real and imaginary part)
                      for each orbital.
        """
        super().__init__(**kwargs)
        self.self_energy = self_energy
        self.freqs = frequencies
        self.norb = self.self_energy.shape[0]
        self.nfreq = self.freqs.shape[0]
        self._projectors = None
        self._new_projectors_loaded = False
        self._self_energy_bandbasis = None

    def plot(self):
        """Plot the real and imaginary part of the self energy.
        """
        plot_multiple_self_energy((self, ))

    @classmethod
    def read(cls, _file, ftype='.dat', units_factor=1,
             freq_min=None, freq_max=None, **kwargs):
        """Load the self energy from one or many data files.

        Parameters
        ----------
        _file : list
                The files paths. One file for each orbital.
        ftype : str, optional, {'.dat'}
                The file format.
        units_factor : float, optional
                       Factor multiplied to the self energy read to have the
                       good units if needed.
        freq_min : float, optional
                   If not None, only frequencies above this minimum will be
                   taken into account.
        freq_max : float, optional
                   Same as freq_min but for a maximum threshold.
        """
        # TODO: find a way to call the cls instance logger instead of creating
        #       one here.
        logging.basicConfig()
        tmplogger = logging.getLogger(cls._loggername)
        tmplogger.setLevel(kwargs.get("loglevel", logging.INFO))
        if isinstance(_file, str):
            # only one file
            _file = (_file, )
        selfenergy = []
        oldfreqs = None
        if ftype == '.dat':
            tmplogger.info("Loading the object from files %s" %
                           ' '.join(map(str, _file)))
            tmplogger.warning("Be careful to have the right"
                              " order for the orbitals!")
            for n, f in enumerate(_file):
                selfE = np.transpose(np.loadtxt(f))
                if not n:
                    oldfreqs = selfE[0]
                # check the frequency grid are all the same
                if n:
                    if not (oldfreqs == selfE[0]).all():
                        raise ValueError("The frequency grids for"
                                         " different orbitals are different."
                                         " We do not"
                                         " treat this problem at the moment.")
                selfenergy.append(selfE[1] + 1j * selfE[2])
        elif ftype == ".SelfEn":
            tmplogger.info("Loading the object from files %s" %
                           ' '.join(map(str, _file)))
            tmplogger.warning("Be careful to have the right"
                              " order for the orbitals!")
            with open(_file[0], "r") as f:
                norb = int(f.readline())
                beta = float(f.readline())
                nfreq = int(f.readline())

                tmplogger.info("Norb: %d; Beta: %f; Nfreq: %d" % (norb, beta,
                               nfreq))
                selfenergy = np.zeros((norb**2, nfreq), dtype=complex)
                freqs = np.zeros((nfreq))

                for w in range(nfreq):
                    line = f.readline().split()
                    freqs[w] = float(line[1])
                    for l in range(norb**2):
                        selfenergy[l][w] = (float(line[2*(l+1)]) +
                                            1j*float(line[2*(l+1)+1]))
            freqs = [freqs]
        else:
            raise NotImplementedError("%s file type not valid." % ftype)
        # check for thresholds
        if freq_min is not None:
            # get only frequencies above this threshold
            where = np.where(oldfreqs >= freq_min)[0]
            # convert all self energy
            selfenergy = [x[where] for x in selfenergy]
            oldfreqs = oldfreqs[where]
        if freq_max is not None:
            where = np.where(oldfreqs <= freq_max)[0]
            selfenergy = [x[where] for x in selfenergy]
            oldfreqs = oldfreqs[where]
        self_energy = np.array(selfenergy) * units_factor
        tmplogger.info("Loading of self energy done")
        return cls(selfE[0] * units_factor, self_energy, **kwargs)

    def write(self, type=".SelfEn", outfile="system.SelfEn"):
        """Writes the Self energy data into a file.

        """
        with open(outfile, "w") as f:
            norb = self.norb/3
            f.write("%d\n" % norb)
            f.write("%f\n" % 116.0)
            f.write("%d\n" % self.nfreq)

            for i in range(self.nfreq):
                line = "%d  %.12f" % (i, self.freqs[i])
                for n in range(self.norb):
                    line += "  %.12f  %.12f" % (self.self_energy[n][i].real,
                                                self.self_energy[n][i].imag)
                line += "\n"
                f.write(line)

    def load_projectors(self, projectors):
        """Sets up the projectors used to project the self energy in
        orbital basis to the band/kpt basis.

        Parameters
        ----------
        projectors : Projectors object
                     It must be a Projectors instance. If not, a
                     TypeError is raised.
        """
        if not isinstance(projectors, Projectors):
            raise TypeError("projectors must be a Projectors instance.")
        self._logger.info("Verifying the projectors...")
        if self.norb != projectors.norb:
            raise ValueError("The projectors do not"
                             " have the same number of orbital as"
                             " the Self energy.")
        self._logger.info("Projectors are OK.")
        self._projectors = projectors
        self._new_projectors_loaded = True

    @property
    def self_energy_bandbasis(self):
        """The self energy in the band basis.
        """
        if self._self_energy_bandbasis is None or self._new_projectors_loaded:
            self._logger.info("Computing projection...")
            self._projectBandBasis()
            self._new_projectors_loaded = False
        return SelfE(self.freqs, self._self_energy_bandbasis,
                     loglevel=self._logger.level)

    def _projectBandBasis(self):
        """Computes the self energy projection in the
        band basis. The projectors must be previously loaded with the
        `load_projectors` method.
        """
        proj = self._projectors
        if proj is None:
            raise ValueError("Projectors must be loaded before"
                             " projection is computed!")
        self._logger.info("Creating self energy array in band basis.")
        self._logger.info("Dimensions: %i x %i x %i x %i => max ~ %.2f"
                          " Gb needed." %
                          (self.nfreq, proj.nkpt, proj.nband, proj.nband,
                           self.nfreq * proj.nkpt * proj.nband *
                           proj.nband * 2 * 64 / 1024 ** 3))
        selfband = np.zeros((self.nfreq, proj.nkpt, proj.nband, proj.nband),
                            dtype=complex)
        # compute projections
        self._logger.info(r"[\Sigma(k,w)_ab = \sum_m <\Psi_ka| \chi_km>"
                          r"<\chi_km|\Psi_kb> dSw_mm")
        pp = proj.projectors
        for a in range(proj.nband):
            for b in range(proj.nband):
                # multiply the projectors for each kpt
                # each proj array is shape = nkpt x norb
                # x is shape nkpt x norb
                x = np.multiply(pp[:, :, a], np.conj(pp[:, :, b]))
                # we want to multiply each x with the self energy of each orb
                # self energy is shape nfreq x norb
                # so for each kpt and freq, we need to multiply x[kpt] and
                # self[w]. We also want to vectorize this and finally, sum on
                # the orbitals.
                # np.einsum does the broadcasting and the multiplication
                prod = np.einsum('ko,ow->wk', x, self.self_energy)
                selfband[:, :, a, b] = prod
        self._logger.info("Calculation done,"
                          " returning SelfE in band basis.")
        self._self_energy_bandbasis = selfband

    def _get_data(self):
        return self.self_energy
