import numpy as np
from itertools import product as itp


class Gamma:
    def __init__(self, ndim, U, J, Up=None, Upp=None, Jp=None, spindiag=True):
        """Object for the vertex function. This represents the interactions
           between two particles or between a particle and a hole. Two incoming
           and two outcoming, so four basis index. In RPA, no dependence on
           k or w but maybe eventually.

        Parameters
        ----------
        ndim : int
               One-particle basis dimension.
        U : float
            Value for on-site interaction.
        J : float
            Value for Hund's coupling.
        Up : float
             Value for (???). Usually Up = U - 2J or Up = U - 5/2J.
        Jp : float
             Value for (???pair hoping or spin-flip???). Usually Jp = J.
        """
        print("Creating vertex function object (Gamma).")
        self.ndim = ndim
        self.U = U
        self.J = J
        self.spindiag = spindiag

        if Up is None:
            self.Up = U - 2*J
        else:
            self.Up = Up
        if Upp is None:
            self.Upp = self.Up - J
        else:
            self.Upp = Upp
        if Jp is None:
            self.Jp = J
        else:
            self.Jp = Jp

        if spindiag:
            self.gammaD = np.zeros((ndim**2, ndim**2))
            self.gammaM = np.zeros((ndim**2, ndim**2))
        else:
            self.gamma = np.zeros((ndim**2*2**2, ndim**2*2**2))
            self.gamma2 = np.zeros((ndim**2*2**2, ndim**2*2**2))

    def constructRPA(self):
        """Constructing RPA vertex function.
        """
        print("Consructing RPA vertex function.")
        if self.spindiag:
            dim = self.ndim**2
            print("Case of a spin-diagonal interaction.")
            for L1, L2 in itp(range(dim), range(dim)):
                l1 = int(L1 / self.ndim)
                l2 = L1 % self.ndim

                for L2 in range(dim):
                    l3 = int(L2 / self.ndim)
                    l4 = L2 % self.ndim

                    if l1 == l2 and l1 == l3 and l1 == l4:
                        self.gammaD[L1, L2] = self.U
                        self.gammaM[L1, L2] = self.U
                    elif l1 == l3 and l2 == l4 and l1 != l2:
                        self.gammaD[L1, L2] = -self.Up + 2*self.J
                        self.gammaM[L1, L2] = self.Up
                    elif l1 == l2 and l3 == l4 and l1 != l3:
                        self.gammaD[L1, L2] = 2*self.Up - self.J
                        self.gammaM[L1, L2] = self.J
                    elif l1 == l4 and l2 == l3 and l1 != l2:
                        self.gammaD[L1, L2] = self.Jp
                        self.gammaM[L1, L2] = self.Jp
                    else:
                        self.gammaD[L1, L2] = 0
                        self.gammaM[L1, L2] = 0
        else:
            dim = self.ndim**2*2**2
            print("Case of a non-spin-diagonal interaction.")
            for L12, L34 in itp(range(dim), range(dim)):
                ls1 = int(L12 / (2*self.ndim))
                ls2 = int(L12 % (2*self.ndim))
                l1 = int(ls1 % self.ndim)
                s1 = int(ls1 / self.ndim)
                l2 = int(ls2 % self.ndim)
                s2 = int(ls2 / self.ndim)

                ls3 = int(L34 / (2*self.ndim))
                ls4 = int(L34 % (2*self.ndim))
                l3 = int(ls3 % self.ndim)
                s3 = int(ls3 / self.ndim)
                l4 = int(ls4 % self.ndim)
                s4 = int(ls4 / self.ndim)

#                 print("L12:", L12, "; ls1:", ls1, "; s1:", s1, "; l1:", l1,
#                       "; ls2:", ls2, "; s2:", s2, "; l2:", l2)
#                 print("L34:", L34, "; ls3:", ls3, "; s3:", s3, "; l3:", l3,
#                       "; ls4:", ls4, "; s4:", s4, "; l4:", l4)

                # Same spin
                if s1 == s2 and s1 == s3 and s1 == s4:
                    if l1 == l2 and l3 == l4 and l1 != l3:
                        self.gamma[L12, L34] = self.Upp
                    elif l1 == l3 and l2 == l4 and l1 != l2:
                        self.gamma[L12, L34] = -self.Upp
                    else:
                        self.gamma[L12, L34] = 0

                # s1 = s4 = -s2 = -s3
                elif s1 == s2 and s3 == s4 and s1 != s3:
                    if l1 == l2 and l1 == l3 and l1 == l4:
                        self.gamma[L12, L34] = self.U
                    elif l1 == l2 and l3 == l4 and l1 != l3:
                        self.gamma[L12, L34] = self.Up
                    elif l1 == l3 and l2 == l4 and l1 != l2:
                        self.gamma[L12, L34] = self.J
                    elif l1 == l4 and l2 == l3 and l1 != l2:
                        self.gamma[L12, L34] = self.Jp
                    else:
                        self.gamma[L12, L34] = 0

                # s1 = s3 = -s2 = -s4
                elif s1 == s3 and s2 == s4 and s1 != s2:
                    if l1 == l2 and l1 == l3 and l1 == l4:
                        self.gamma[L12, L34] = -self.U
                    elif l1 == l3 and l2 == l4 and l1 != l2:
                        self.gamma[L12, L34] = -self.Up
                    elif l1 == l2 and l3 == l4 and l1 != l3:
                        self.gamma[L12, L34] = -self.J
                    elif l1 == l4 and l2 == l3 and l1 != l2:
                        self.gamma[L12, L34] = -self.Jp
                    else:
                        self.gamma[L12, L34] = 0

                else:
                    self.gamma[L12, L34] = 0

            np.set_printoptions(linewidth=np.inf, threshold=np.inf)

    def checkCrossingSym(self):
        print("Checking crossing relations.")
        rel1, rel2 = True, True
        if not self.spindiag:
            for ls1, ls2, ls3, ls4 in itp(range(2*self.ndim), repeat=4):
                L12 = ls1*self.ndim*2 + ls2
                L34 = ls3*self.ndim*2 + ls4

                L42 = ls4*self.ndim*2 + ls2
                L31 = ls3*self.ndim*2 + ls1
                if (self.gamma[L12, L34] + self.gamma[L42, L31] != 0).any():
                    rel1 = False

                L43 = ls4*self.ndim*2 + ls3
                L21 = ls2*self.ndim*2 + ls1
                if (self.gamma[L12, L34] - self.gamma[L43, L21] != 0).any():
                    rel2 = False
        if not rel1:
            print("ERROR! Crossing rel1 (Gamma[12,34] = -Gamma[42,31]) "
                  "failed!")
        if not rel2:
            print("ERROR! Crossing rel2 (Gamma[12,34] =  Gamma[43,21]) "
                  "failed!")

    def __getitem__(self, key):
        if not key:
            return self.gammaD
        elif key == 1:
            return self.gammaM

    def __setitem__(self, key, value):
        self.gammaD[key] = value[0]
        self.gammaM[key] = value[1]
