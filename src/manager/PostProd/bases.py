import abc
import logging


class PostProdBase(abc.ABC):
    """Base class for every object in post prod.

    For now, it only sets up the logger.
    """
    _loggername = None

    def __init__(self, loglevel=logging.INFO):
        if self._loggername is None:
            raise NotImplementedError("This PostProd object don't have a"
                                      " loggername, this should be"
                                      " implemented.")
        logging.basicConfig()
        self._logger = logging.getLogger(self._loggername)
        self._logger.setLevel(loglevel)

    @abc.abstractmethod
    def _get_data(self):
        pass

    @property
    def _data(self):
        return self._get_data()

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value
