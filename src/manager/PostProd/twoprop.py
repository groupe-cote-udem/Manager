import numpy as np
from numpy import linalg as ln
from .physobj import PhysObj
from .gamma import Gamma
# from .kptmesh import KptMesh
# from .gap import Gap
from itertools import product as itp


class TwoPropagator():
    def __init__(self, ndim, nfreq, nkpt, spindiag=True):
        """Object description a two-body propagator. It implies two incoming
           objects and two outcoming ones. So in a given one-body basis {l},
           for either particle-particle (pp) or particle-hole (ph), it is an
           object such that
           [G^{pp/ph}(k, w)]_{l_1l_2;l_3l_4}

        Parameters
        ----------
        ndim : int
               Dimension of a one-body propagators, of the basis.
        nfreq : int
                Number of frequencies of the object.
        nkpt : int
               Number of k points.
        spindiag : bool
                   Whether the object is treated in a spin-diagonal fashion
                   or not.
        """
        self.Re = PhysObj(ndim**2, nkpt, nfreq)
        self.Im = PhysObj(ndim**2, nkpt, nfreq)

        self.ndim = ndim
        self.nfreq = nfreq
        self.nkpt = nkpt
        self.spindiag = spindiag

    def dressing(self, gamma_ph, nspin=2, returnEig0=True, flag_nostop=False):
        r"""From a vertex Gamma, dresses the two-particle propagator.
        Also print the Stoner factors. This is used in the non-spin-
        diagonal case right now.

        Parameters
        ----------
        gamma : Gamma
                This object is a matrix with the same dimensions as self. It
                encodes the two-particle interactions.
        nspin : int
                This should be given in the __init__ function.
        """
        norb = int(self.ndim/nspin)
        nfreqs = self.nfreq
        nqpts = self.nkpt

        if not isinstance(gamma_ph, Gamma):
            raise TypeError("The gamma argument has to be of type Gamma!")

        dchi_ph = TwoPropagator(nspin*norb, nfreqs, nqpts, spindiag=False)
        stoner = np.zeros((nqpts, nfreqs), dtype=complex)
        eig_vals = np.zeros((nqpts, nfreqs, 5), dtype=complex)
        maxs, mins = (np.zeros((nspin**2*norb**2)),
                      np.zeros((nspin**2*norb**2)))

        for k, w in itp(range(nqpts), range(nfreqs)):
            barechi = self.Re[k, w] + 1j*self.Im[k, w]
            gammachi = gamma_ph.gamma.dot(barechi)
            denom = ln.inv(np.eye(norb**2*nspin**2) + gammachi)
            dressed = barechi.dot(denom)
            dchi_ph.Re[k, w] = dressed.real
            dchi_ph.Im[k, w] = dressed.imag

            if returnEig0:
                gammachi2 = np.copy(gammachi)
                A, B = dchi_ph.changeBasis(1, "OSOS", "OOSS")
                gammachi2[A, :] = gammachi2[B, :]
                gammachi2[:, A] = gammachi2[:, B]

                A = (list(range(norb**2, 2*norb**2)) +
                     list(range(3*norb**2, 4*norb**2)))
                B = (list(range(3*norb**2, 4*norb**2)) +
                     list(range(norb**2, 2*norb**2)))
                gammachi2[A, :] = gammachi2[B, :]
                gammachi2[:, A] = gammachi2[:, B]

                eigenValues = ln.eigvals(-gammachi2)
                idx = eigenValues.argsort()[::-1]
                eigenValues = eigenValues[idx]
                eig_vals[k, w, 0] = eigenValues[0]

                A1, A2 = norb**2, 2*norb**2
                subGammaChi = -gammachi2[:A1, :A1]-gammachi2[:A1, A1:A2]
                eigenValues = ln.eigvals(subGammaChi)
                idx = eigenValues.argsort()[::-1]
                eigenValues = eigenValues[idx]
                eig_vals[k, w, 1] = eigenValues[0]

                subGammaChi = -gammachi2[:A1, :A1]+gammachi2[:A1, A1:A2]
                eigenValues = ln.eigvals(subGammaChi)
                idx = eigenValues.argsort()[::-1]
                eigenValues = eigenValues[idx]
                eig_vals[k, w, 2] = eigenValues[0]

                A = [2*norb**2, 3*norb**2]
                subGammaChi = -gammachi2[A[0]:A[1], A[0]:A[1]]
                eigenValues = ln.eigvals(subGammaChi)
                idx = eigenValues.argsort()[::-1]
                eigenValues = eigenValues[idx]
                eig_vals[k, w, 3] = eigenValues[0]

                A = [3*norb**2, 4*norb**2]
                subGammaChi = -gammachi2[A[0]:A[1], A[0]:A[1]]
                eigenValues = ln.eigvals(subGammaChi)
                idx = eigenValues.argsort()[::-1]
                eigenValues = eigenValues[idx]
                eig_vals[k, w, 4] = eigenValues[0]

            stoner[k, w] = np.amax(ln.eigvals(-gammachi).real)

            for l1 in range(norb**2*nspin**2):
                v = dressed[l1, l1]
                if v > maxs[l1]:
                    maxs[l1] = v
                if v < mins[l1]:
                    mins[l1] = v

        print(stoner[:, 0])
        print("Max Stoner: ", np.amax(stoner))

        print("Maxs of dressed chi: ")
        print(maxs)
        print()
        print("Mins of dressed chi: ")
        print(mins)

        if np.amin(mins) < 0 and flag_nostop is False:
            raise ValueError("Dressed susceptibility has a negative value."
                             " This is usually an indication of a Stoner "
                             "factor higher than 1.")

        if returnEig0:
            return dchi_ph, eig_vals
        else:
            return dchi_ph

    def changeBasis(self, obj, basis1="OSOS", basis2="OOSS", norb=3):
        r"""Definitions of basis:

            OSOS:
                |l_1> otimes |\sigma_1> otimes |l_2> otimes |\sigma_2>

            OOSS:
                |l_1> otimes |l_2> otimes |\sigma_1> otimes |\sigma_2>

            OPOP:
                |l_1> otimes |\rho_1> otimes |l_2> otimes |\rho_2>

            OOPP:
                |l_1> otimes |l_2> otimes |\rho_1> otimes |\rho_2>

            where l, \sigma, \rho are orbital, spin and pseudospin labels.

        Parameters
        ----------
        obj : Matrix of shape still to define

        basis1, basis2 : str
                         Choice of label defined above.

        norb : Int
               Is 3 for now.
        """
        if norb != 3:
            raise NotImplementedError("Only norb=3 case is implemented. "
                                      "Generalization TODO.")

        cobj = np.copy(obj)

        cond = ((basis1 == "OSOS" and basis2 == "OOSS") or
                (basis1 == "OOSS" and basis2 == "OSOS"))
        if cond:
            ls2sl = np.zeros((6, 6))
            ls2sl[0, 0], ls2sl[5, 5] = 1, 1
            ls2sl[1, 2], ls2sl[2, 4], ls2sl[3, 1], ls2sl[4, 3] = 1, 1, 1, 1

            sl2ls = np.linalg.inv(ls2sl)

            id3 = np.eye(3)
            id2 = np.eye(2)

            R = np.kron(np.kron(id2, sl2ls), id3)
            Ri = np.linalg.inv(R)

            A = list(range(6*6))
            B = [0 for i in range(6*6)]
            for a in A:
                b = np.where(Ri[a] == 1)
                B[a] = b[0][0]

            if basis1 == "OSOS":
                return A, B
            else:
                return B, A

        elif basis1 == "OSOS" and basis2 == "OPOP":
            cobj = np.copy(obj)

            os2op = np.zeros((6, 6))
            os2op[0, 3], os2op[3, 0] = 1, 1
            os2op[1, 1], os2op[2, 2], os2op[4, 4], os2op[5, 5] = 1, 1, 1, 1

            R = np.kron(os2op, os2op)

            A = list(range(6*6))
            B = [0 for i in range(6*6)]
            for a in A:
                b = np.where(R[a] == 1)
                B[a] = b[0][0]

            return A, B

        elif basis1 == "OSOS" and basis2 == "OOPP":
            A, B = self.changeBasis(obj, basis1="OSOS", basis2="OPOP")
            A2, B2 = self.changeBasis(obj, basis1="OSOS", basis2="OOSS")

            R1, R2 = np.zeros((36, 36)), np.zeros((36, 36))
            for i in range(36):
                R1[A[i], B[i]] = 1
                R2[A2[i], B2[i]] = 1

            R = R2.dot(R1)

            A3 = list(range(6*6))
            B3 = [0 for i in range(6*6)]
            for a3 in A3:
                b3 = np.where(R[a3] == 1)
                B3[a3] = b3[0][0]
            return A3, B3

        elif basis1 == "OOSS" and basis2 == "OOPP":
            A, B = self.changeBasis(obj, basis1="OOSS", basis2="OSOS")
            A2, B2 = self.changeBasis(cobj, basis1="OSOS", basis2="OOPP")
            # assert((A == A2).all())

            B3 = np.copy(B)
            B3[A] = B3[B2]

            return A, B3

    def returnInPseudospin(self, norb=3):
        r"""The basis coming from the BareChi program for a TwoParticle object,
            a two-particle object where each particle has for example
            spin in {u, d} and orb in {x, y}, would probably be given has

            {(xu;xu), (xu;yu), (xu;xd), (xu;yd),
             (yu,...),
             (xd,...),
             (yd,...)}.

            This function returns the object is the pseudospin basis.
        Parameters
        ----------
        norb : Int
               Number of orbital. Not given in definition but should.
               Only norb=3 works right now, not generalized.
        """

        if norb != 3:
            raise NotImplementedError("Only norb=3 case is implemented. "
                                      "Generalization TODO.")

        chi_pseudo = np.copy(self.Re.Obj + 1j*self.Im.Obj)

        A, B = self.changeBasis(chi_pseudo, basis1="OSOS", basis2="OPOP")

        chi_pseudo[:, :, A, :] = chi_pseudo[:, :, B, :]
        chi_pseudo[:, :, :, A] = chi_pseudo[:, :, :, B]
        return chi_pseudo

    def returnInSpin(self, norb=3):
        r"""The basis coming from the BareChi program for a TwoParticle object,
            a two-particle object where each particle has for example
            spin in {u, d} and orb in {x, y}, would probably be given has

            {(xu;xu), (xu;yu), (xu;xd), (xu;yd),
             (yu,...),
             (xd,...),
             (yd,...)}.

            This function returns the object is the basis

            {(xu;xu), (xu;yu), (yu;xu), (yu;yu),
             (...u,...d),
             (...d,...u),
             (...d,...d)}
            which is more easy to spin-diagonalize.

        Parameters
        ----------
        norb : Int
               Number of orbital. Not given in definition but should.
               Only norb=3 works right now, not generalized.
        """

        if norb != 3:
            raise NotImplementedError("Only norb=3 case is implemented. "
                                      "Generalization TODO.")

        chi_spin = np.copy(self.Re.Obj + 1j*self.Im.Obj)

        A, B = self.changeBasis(chi_spin, basis1="OSOS", basis2="OOSS")

        chi_spin[:, :, A, :] = chi_spin[:, :, B, :]
        chi_spin[:, :, :, A] = chi_spin[:, :, :, B]
        return chi_spin

    def partialTranspose(self, obj, orbs, o_norb=3):
        r"""Applies full or partial transpose. The twopropagators have
            4 spin and 4 orbital indices. It can be useful to transpose
            1 and 3, 2 and 4 or both.

        Parameters
        ----------
        obj : nparray
            Four dimensional numpy array. First two dimensions with arbitrarly
            length (nkpt, nfreqs), then two other dimensions with length
            (o_norb**2, n_orb**2).
        orbs : str
               Type of transpose. Choices are "13", "24", "1234".
        """
        print(len(obj[0][0]), o_norb**2)
        nobj = np.zeros((len(obj), len(obj[0]), o_norb**2, o_norb**2),
                        dtype=complex)
        for i, j in itp(range(len(obj)), range(len(obj[0]))):
            o = np.copy(obj[i][j])
            if orbs == "13" or orbs == "1234":
                o = np.transpose(o)
            if orbs == "13" or orbs == "24":
                for l1, l3 in itp(range(o_norb), range(o_norb)):
                    o1i, o1f = o_norb*l1, o_norb*(l1+1)
                    o3i, o3f = o_norb*l3, o_norb*(l3+1)
                    o[o1i:o1f, o3i:o3f] = np.transpose(o[o1i:o1f, o3i:o3f])
            nobj[i, j] = o
        return nobj

    def leadingInstabilities(self, gamma, nlead=1):
        r"""Returns the nlead leading k point where the instability is more
           diverging. When dressing the bare propagators with a vertex
           function, divergences occur when \pm \Gamma\G_0^{ph} approaches
           unity, + for magnetic channel and - for density channel.

        Parameters
        ----------
        gamma : Gamma
                This object is a matrix with the same dimensions as self. It
                encodes the two-particle interactions.
        nlead : int
                Number of leading k points returned.
        """
        eig = np.zeros((2, self.nkpt, self.nfreq), dtype=complex)
        for k in range(self.nkpt):
            for w in range(self.nfreq):
                den = -1*gamma[0].dot(self[k, w])
                mag = gamma[1].dot(self[k, w])
                """
                print(gamma[0])
                print(den)
                print(gamma[1])
                print(mag)
                """
                eig[0, k, w] = np.amax(np.linalg.eigvals(den))
                eig[1, k, w] = np.amax(np.linalg.eigvals(mag))
                # print("k : %d; w: %d; Density: %f+j%f; Magnetic: %f+j%f" %
                #       (k, w, eig[0, k, w].real, eig[0, k, w].imag,
                #       eig[1, k, w].real, eig[1, k, w].imag))

        index_den = eig[0, :, 0].argsort(axis=0)
        index_mag = eig[1, :, 0].argsort(axis=0)

        print(den)

        print(eig[0, :, 0].real)
        print(index_den)

        print("Density:")
        k_den = []
        for i in range(1, nlead+1):
            k = index_den[-i]
            k_den.append(k)
            print("rank: %d; k index: %d; value: %f+i%f" %
                  (i, k, eig[0, k, 0].real, eig[0, k, 0].imag))

        print("Magnetic:")
        k_mag = []
        for i in range(1, nlead+1):
            k = index_mag[-i]
            k_mag.append(k)
            k = index_mag[-i]
            print("rank: %d; k index: %d; value: %f+i%f" %
                  (i, k, eig[1, k, 0].real, eig[1, k, 0].imag))

        return eig, [k_den, k_mag]

    def dimSubMatrix(self, sub_index, write=None):
        """Returns a sub matrix for each k and w.

        Parameters
        ----------
        sub_index : int list
                    One-particle states to keep. The TwoPropagator as 4 indices
                    here only given the incices such as [1, 2] for ndim = 3.
        write : str
                Filename of output.
        """
        sub = TwoPropagator(len(sub_index), self.nfreq, self.nkpt)

        ll = []
        for l1, l2 in itp(sub_index, sub_index):
            ll.append(l1*self.ndim+l2)

        sub.Re.Obj = self.Re[np.ix_(range(self.nkpt), range(self.nfreq),
                             ll, ll)]
        sub.Im.Obj = self.Im[np.ix_(range(self.nkpt), range(self.nfreq),
                             ll, ll)]

        return sub

    def __getitem__(self, key):
        return self.Re[key] + 1j*self.Im[key]

    def __setitem__(self, key, value):
        self.Re[key] = value.real
        self.Im[key] = value.imag

    def __len__(self):
        return len(self.Re.Obj)
