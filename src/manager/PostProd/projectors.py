from abisuite import AbinitDMFTProjectorsFile
import numpy as np
import scipy.linalg as ln
from itertools import product as itp
from math import pi, cos, sin, sqrt

from .bases import PostProdBase


class Projectors(PostProdBase):
    _loggername = "postprod.projectors"

    def __init__(self, norb, nband, nkpt, natom=1, nspin=1, **kwargs):
        """Object that represents the projectors onto the orbital basis.

        Parameters
        ----------
        norb : int
               The number of orbitals.
        nband : int
                The number of bands.
        nkpt : int
               The number of k-points.
        natom : int
                The number of correlated atoms.
        nspin : int
                The number of spins.
        """
        super().__init__(**kwargs)

        self.natom = natom
        self.nspin = nspin

        if isinstance(norb, int):
            self.norb = [norb for x in range(natom)]
        else:
            if len(norb) != natom:
                raise ValueError("Badly defined norb for multi-atom case.")
            self.norb = norb
        self.nband = nband
        self.nkpt = nkpt

        self.projectors = [np.zeros((nkpt, nspin, self.norb[a], nband),
                                    dtype=complex) for a in range(natom)]

        self._logger.info("Projectors well initiated and has dimensions "
                          "[(nkpt, nspin, norb, nband) for natom] = %s."
                          % str([self.projectors[a].shape
                                 for a in range(natom)]))

    def read(self, _file, **kwargs):
        """Reads the projectors from a file.

        Parameters
        ----------
        _file : str
                The path to the projectors file.
        """
        self._logger.info("Loading the projectors from the file %s." %
                          _file)
        data = []
        with AbinitDMFTProjectorsFile.from_file(_file, **kwargs) as proj_file:
            # need to swap band and orb axes
            assert(self.natom == len(proj_file.projectors))
            for a in range(self.natom):
                da = np.swapaxes(proj_file.projectors[a], 1, 3)
                da = np.swapaxes(da, 1, 2)
                data.append(da)
                assert(self.projectors[a].shape == data[a].shape)
        # for now assume 1 spin and 1 atom only
        for a in range(self.natom):
            self[a][:, :, :, :] = data[a][:, :, :, :]
        self._logger.info("Projectors loaded!")

    def orthonormalize(self, otype=0):
        """Orthonormalizes the projectors.
        otype : int
                Defines the type of orthonormalization procedure used. If 0,
                orthonormalizes for atoms as well. If 1, each atom done
                separately.
        """
        self._logger.info("Orthonormalization of the projectors.")

        if self.nspin > 1:
            self._logger.info("The orthomalization in the multispin systems "
                              "is not clearly the right one.")
        nproj = Projectors(self.norb, self.nband, self.nkpt, natom=self.natom,
                           nspin=self.nspin)

        if otype == 0:
            pre_norb = []
            i = 0
            for a in range(self.natom):
                pre_norb.append(i)
                i += self.norb[a]
            pre_norb.append(i)
            print(pre_norb)
            # TODO: improve the vectorization of this calculation
            for k, s in itp(range(self.nkpt), range(self.nspin)):
                print(k, s)
                Op = np.zeros((sum(self.norb), sum(self.norb)), dtype=complex)
                for a1, a2 in itp(range(self.natom), range(self.natom)):
                    projk1 = self[a1][k, s]
                    projk2 = self[a2][k, s]
                    for n1, n2 in itp(range(self.norb[a1]),
                                      range(self.norb[a2])):
                        print(n1, n2, a1, a2, k, s)
                        print(projk1[n1])
                        print(projk2[n2])
                        prod = projk1[n1].dot(np.conj(projk2[n2]))
                        print(prod)
                        Op[pre_norb[a1]+n1, pre_norb[a2]+n2] = prod

                print("Before S")
                S = ln.sqrtm(ln.inv(Op))
                print("After S")

                for a1, a2 in itp(range(self.natom), range(self.natom)):
                    projk = self[a2][k, s]
                    for n1, b in itp(range(self.norb[a1]), range(self.nband)):
                        pn1, pn2, pn3 = (pre_norb[a1], pre_norb[a2],
                                         pre_norb[a2+1])
                        toadd = S[pn1+n1, pn2:pn3].dot(projk[:, b].T)
                        nproj[a1][k, s, n1, b] += toadd

            self.projectors = nproj.projectors
            self._logger.info("Orthonormalization done!\n")

        elif otype == 1:
            for k, s in itp(range(self.nkpt), range(self.nspin)):
                for a in range(self.natom):
                    Op = np.zeros((self.norb[a], self.norb[a]), dtype=complex)
                    p = self[a][k, s]
                    for n1, n2 in itp(range(self.norb[a]),
                                      range(self.norb[a])):
                        prod = p[n1].dot(np.conj(p[n2]))
                        Op[n1, n2] = prod

                    S = ln.sqrtm(ln.inv(Op))

                    p = self[a][k, s]
                    for n1, b in itp(range(self.norb[a]), range(self.nband)):
                        toadd = S[n1, :].dot(p[:, b].T)
                        nproj[a][k, s, n1, b] += toadd

            self.projectors = nproj.projectors
            self._logger.info("Orthonormalization done!\n")

    def check_orthonormalization(self, klist='first', prec=8, otype=0):
        """Checks that the orthonormalization is correct.

        Parameters
        ----------
        klist : str, optional, {'first', 'all'}
                If set to 'first' the checkup is done only on the first kpt.
                If set to 'all', the checkup is done on all kpts.
        """
        if klist == "first":
            klist = [0]
        elif klist == "all":
            klist = range(self.nkpt)
        else:
            raise ValueError("klist argument should be 'first' or 'all'.")
        results = []
        alert = 0
        # if self.nspin > 1:
        #     raise ValueError("Projector has more than one spin. "
        #                      "Orthonormalization procedure is not yet "
        #                      "generalized to multispin systems.")
        if otype == 0:
            tot_norb = sum(self.norb)
            pre_norb = []
            i = 0
            for a in range(self.natom):
                pre_norb.append(i)
                i += self.norb[a]
            pre_norb.append(i)

            # TODO: improve the vectorization of this calculation
            for k, s in itp(klist, range(self.nspin)):
                for a1, a2 in itp(range(self.natom), range(self.natom)):
                    _id = np.zeros((tot_norb, tot_norb), dtype=complex)
                    projk1 = self[a1][k, s]
                    projk2 = self[a2][k, s]
                    for n1, n2 in itp(range(self.norb[a1]),
                                      range(self.norb[a2])):
                        pn1, pn2 = pre_norb[a1], pre_norb[a2]
                        p1, p2 = projk1[n1], np.conj(projk2[n2])
                        _id[pn1+n1, pn2+n2] += p1.dot(p2)
                    np.set_printoptions(linewidth=132, precision=5)
                    if not alert:
                        if np.max(np.around(_id, prec) -
                                  np.identity(tot_norb)) > 0:
                            alert = 1
                    results.append(np.around(_id, prec))
        if otype == 1:
            for k, s in itp(klist, range(self.nspin)):
                for a1 in range(self.natom):
                    _id = np.zeros((self.norb[a1], self.norb[a1]),
                                   dtype=complex)
                    p1 = self[a1][k, s]
                    p2 = self[a1][k, s]
                    for n1, n2 in itp(range(self.norb[a1]),
                                      range(self.norb[a1])):
                        _id[n1, n2] += p1[n1].dot(np.conj(p2[n2]))
                    np.set_printoptions(linewidth=132, precision=5)
                    if not alert:
                        if np.max(np.around(_id, prec) -
                                  np.identity(self.norb[a1])) > 0:
                            alert = 1
                    results.append(np.around(_id, prec))
        if alert:
            message = "NOT GOOD."
            np.set_printoptions(threshold=np.inf, linewidth=np.inf,
                                precision=1)
            print(results)
        else:
            message = "Well done."

        self._logger.info("Checked orthonormalization: %s" % message)
        return results

    def returnSubset(self, orbs=None, bands=None):
        if orbs is None and bands is None:
            self._logger.info("returnSubset: nor orbs nor bands defined.")
            return self
        elif orbs is not None and bands is None:
            self._logger.info("returnSubset: taking the following subset of "
                              "orbitals: %s" % orbs)
            bands = range(self.nband)
        elif bands is not None and orbs is None:
            self._logger.info("returnSubset: taking the following subset of "
                              "bands: %s" % bands)
            orbs = [range(self.norb[a]) for a in range(self.natom)]
        else:
            self._logger.info("returnSubset: taking the following subset of "
                              "orbs, bands: %s, %s" % (orbs, bands))

        nproj = Projectors([len(orbs[i]) for i in range(self.natom)],
                           len(bands), self.nkpt, natom=self.natom,
                           nspin=self.nspin)
        for a in range(self.natom):
            ix = np.ix_(range(self.nkpt), range(self.nspin),
                        orbs[a], bands)
            nproj.projectors[a] = self.projectors[a][ix]
        return nproj

    def rotateToLocalBasis(self, at, l_at, angles):
        if l_at != 2:
            raise ValueError("The rotation is only implemented for "
                             "d-orbitals for now!")

        self._logger.info("Rotating orbitals for atom %d with "
                          "angular momentum l_at=%d. "
                          "Angles: alpha=%.4f, beta=%.4f, gamma=%.4f."
                          % (at, l_at, angles[0], angles[1], angles[2]))

        if self.norb[at] != 2*l_at+1:
            raise ValueError("The number of orbitals of atom %d is %d != 2*l+1"
                             "! To rotate orbitals, need all from the same "
                             "shell.")
        d = 2*pi/360
        if l_at == 2:
            def Rz(a):
                return np.matrix([[cos(2*a*d), 0, 0, 0, sin(2*a*d)],
                                  [0, cos(a*d), 0, sin(a*d), 0],
                                  [0, 0, 1, 0, 0],
                                  [0, -sin(a*d), 0, cos(a*d), 0],
                                  [-sin(2*a*d), 0, 0, 0, cos(2*a*d)]])

            def Ry(b):
                return np.matrix([[cos(b*d), sin(b*d), 0, 0, 0],
                                  [-sin(b*d), cos(b*d), 0, 0, 0],
                                  [0, 0, .25*(1+3*cos(2*b*d)),
                                   -sqrt(3)*cos(b*d)*sin(b*d),
                                   sqrt(3)/2*sin(b*d)**2],
                                  [0, 0, sqrt(3)*cos(b*d)*sin(b*d),
                                   cos(2*b*d), -cos(b*d)*sin(b*d)],
                                  [0, 0, sqrt(3)/2*sin(b*d)**2,
                                   cos(b*d)*sin(b*d), .25*(3+cos(2*b*d))]])

        nproj = Projectors([self.norb[at]], self.nband, self.nkpt,
                           natom=1, nspin=self.nspin)

        for k, s, b in itp(range(self.nkpt), range(self.nspin),
                           range(self.nband)):
            tmp = np.transpose(np.array([self[at][k, s, :, b]]))
            tmp = Rz(-angles[2])*Ry(-angles[1])*Rz(-angles[0])*tmp
            nproj[0][k, s, :, b] = np.transpose(tmp)
        return nproj[0]

    def _get_data(self):
        return self.projectors
