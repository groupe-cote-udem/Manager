from .kptmesh import KptMesh
from .hamiltonian import Hk
from .gap import Gap
import numpy as np
from itertools import product as itp


class Bogoliubov:
    """
    This class performs the Bogoliubov transformation, from a normal-state
    Hamiltonian and a gap function.
    """
    def __init__(self, hk_uu, hk_dd, gap_ud, gap_du, mesh=None, nfreq=0):
        if not isinstance(hk_uu, Hk):
            raise TypeError("The given hamiltonian needs to be a Hk object.")
        if not isinstance(hk_dd, Hk):
            raise TypeError("The given hamiltonian needs to be a Hk object.")
        if gap_ud is not None and not isinstance(gap_ud, Gap):
            raise TypeError("The given gap function needs to be a Gap object.")
        if gap_du is not None and not isinstance(gap_du, Gap):
            raise TypeError("The given gap function needs to be a Gap object.")
        if mesh is not None and not isinstance(mesh, KptMesh):
            raise TypeError("The given mesh needs to be a KptMesh object.")
        if gap_ud is not None and hk_uu.dim != gap_ud.ndim:
            raise ValueError("Hamiltonian and gap functions have different "
                             "dimensionality.")

        self.hk_uu = hk_uu
        self.hk_dd = hk_dd

        self.ndim = 2*hk_uu.dim

        if mesh is None:
            self.mesh = hk_uu.mesh
        else:
            self.mesh = mesh

        if gap_ud is None and gap_du is None:
            gap_ud = Gap(hk_uu.dim, self.mesh, 2)
            gap_du = Gap(hk_uu.dim, self.mesh, 2)
        self.gap_ud = gap_ud
        self.gap_du = gap_du

        self.nfreq = nfreq
        if nfreq == 0:
            self.bogoliubov = np.zeros((self.mesh.nkpt, 1, self.ndim,
                                        self.ndim), dtype=complex)
            self.eigs = np.zeros((self.mesh.nkpt, self.ndim))
        else:
            self.bogoliubov = np.zeros((self.mesh.nkpt, nfreq, self.ndim,
                                        self.ndim), dtype=complex)
            self.eigs = np.zeros((self.mesh.nkpt, nfreq, self.ndim))

    def construct_ham(self, fermi):
        """
        This function take H0 up up, H0 dn dn, Delta and Delta^dag to
        construct Hk, the bogoliubov hamiltonian.
        """
        gap_ud = self.gap_ud
        gap_du = self.gap_du

        gm = self.gap_ud.mesh
        for k, kpt in enumerate(gm.npts):
            gap_dag = np.transpose(np.conj(gap_du[k]), axes=[0, 2, 1])
            fermi = fermi*np.eye(self.hk_uu.dim)

            kx, ky, kz = kpt[0], kpt[1], kpt[2]
            n1 = np.transpose(np.matrix([kx, ky, kz]))
            k1 = n1 - gm.N0
            k2 = -1*k1
            n2 = k2 + gm.N0
            n2 = gm.translate_in_FBZ([n2.item(0), n2.item(1), n2.item(2)])
            k2x, k2y, k2z = (int(round(n2[0])), int(round(n2[1])),
                             int(round(n2[2])))
            k2 = gm[k2x, k2y, k2z]
            # print("k2: ", k2, k2x, k2y, k2z)
            hk_T = np.transpose(self.hk_dd[k2])
            # hk_T = self.hk_dd[k2]

            if self.nfreq > 2:
                raise NotImplementedError("bogo.nfreq > 2 not implemented "
                                          "here.")
            for w in range(self.nfreq):
                self[k, w] = np.block([[self.hk_uu[k]-fermi, gap_dag[w]],
                                       [gap_ud[k, 1-w], -hk_T+fermi]])
            if not self.nfreq:
                self[k, 0] = np.block([[self.hk_uu[k]-fermi, gap_dag[0]],
                                       [gap_ud[k, 0], -hk_T+fermi]])

    def compute_eigs(self):
        self.eigs[:] = np.linalg.eigvals(self[:])

    def return_green(self, omega, broad=0.05, static=True, testing=False):
        print(omega)
        green = np.zeros((self.mesh.nkpt, len(omega), self.ndim, self.ndim),
                         dtype=complex)

        # hkd1 = np.conjugate(np.transpose(self[:, 1], axes=[0, 2, 1]))
        for k, o in itp(range(self.mesh.nkpt), range(len(omega))):
            if static or self.nfreq == 0 or self.nfreq == 1:
                tmp = (np.linalg.inv((omega[o]+1j*broad)*np.eye(self.ndim)
                       - self[k, 0]))
                green[k, o] = tmp
            else:
                if self.nfreq == 2:
                    if o < len(omega)/2:
                        hkw = self[k, 0]
                    else:
                        hkw = self[k, 1]
                else:
                    raise ValueError("Problem")
                tmp = (np.linalg.inv((omega[o]+1j*broad)*np.eye(self.ndim)
                                     - hkw))
                green[k, o] = tmp
        if testing:
            np.set_printoptions(precision=5)
            hndim = int(self.ndim/2)
            g00g11 = np.zeros((self.mesh.nkpt, len(omega), hndim, hndim),
                              dtype=complex)
            for k, kpt in enumerate(self.mesh.npts):
                for o in range(len(omega)):
                    kx, ky, kz = kpt[0], kpt[1], kpt[2]
                    n1 = np.transpose(np.matrix([kx, ky, kz]))
                    k1 = n1 - self.mesh.N0
                    k2 = -1*k1
                    n2 = k2 + self.mesh.N0
                    n2 = self.mesh.translate_in_FBZ([n2.item(0), n2.item(1),
                                                     n2.item(2)])
                    k2x, k2y, k2z = (int(round(n2[0])), int(round(n2[1])),
                                     int(round(n2[2])))
                    k2 = self.mesh[k2x, k2y, k2z]
                    g00g11[k, o] = (green[k, o][:hndim, :hndim] +
                                    np.conjugate(green[k2, o][hndim:, hndim:]))
                    if k == 0 and o == 0:
                        print(k, o)
                        print(green[k, o])
                        print(green[k, o][:hndim, :hndim])
                        print(green[k2, o][hndim:, hndim:])
            print("max of g00g11")
            print(np.amax(np.amax(g00g11, axis=0), axis=0))
        return green

    def return_delta_w(self):
        iden = np.eye(2*self.hk_uu.dim)
        print(self.nfreq)
        if self.nfreq == 0:
            print(iden)
            return iden
        elif self.nfreq != 2:
            raise ValueError("Only works for nfreq == 0 or 2.")
        else:
            deltaHk = self[:, 1] - self[:, 0]
            print(deltaHk)
            return deltaHk

    def __getitem__(self, key):
        return self.bogoliubov[key]

    def __setitem__(self, key, value):
        self.bogoliubov[key] = value
