from .physobj import PhysObj
import numpy as np
import matplotlib.pyplot as plt
import itertools as it
import re
import mpl_toolkits.axes_grid1.inset_locator as inset_axes
import math
from matplotlib import colors


# matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]


class PlotParams():
    def __init__(self, opt=None):
        self.default = True
        p = {"figsize": (5, 5),
             "title": "",
             "titlesize": 14,
             "xlim": None,
             "ylim": None,
             "xticks": None,
             "yticks": None,
             "xlabels": None,
             "ylabels": None,
             "labelsize": 16,
             "ticksize": 10,
             "tight_layout": False,
             "linestyle": "-",
             "linewidth": 3,
             "show": False,
             "savefig": None,
             "legend_loc": False,
             "legend_fontsize": "small",
             "special1": False,
             "special2": False,
             "bounds": [None, None],
             "sharex": False,
             "sharey": False}
        self.baseParams = p
        self.params = {}

    def setDefaults(self, default):
        if not isinstance(default, PlotParams):
            raise TypeError("plotParams.setDefaults needs a plotParams "
                            "object.")

        for param in default.params:
            if self.default:
                self.params[param] = default[param]
            elif param not in self.params:
                self.params[param] = default[param]
        for param in self.baseParams:
            if param not in self.params:
                self.params[param] = self.baseParams[param]
        self.default = False

    def __getitem__(self, key):
        return self.params[key]

    def __setitem__(self, key, value):
        self.params[key] = value


def setFigure(params, printVariables=None):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    fig, ax = plt.subplots(params["subplots"][0], params["subplots"][1],
                           sharey=params["sharey"], sharex=params["sharex"],
                           figsize=params["figsize"])

    plt.suptitle(params["title"], fontsize=params["titlesize"])
    return fig, ax


def setSubplot(ax, params, printVariables=None):
    if printVariables is None:
        printVariables = {}
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    for a, b in it.product(range(params["subplots"][0]),
                           range(params["subplots"][1])):

        if params["subplots"][0] == 1 and params["subplots"][1] == 1:
            axe = ax
        elif params["subplots"][0] == 1:
            axe = ax[b]
        elif params["subplots"][1] == 1:
            axe = ax[a]
        else:
            axe = ax[a, b]

        plt.sca(axe)

        if params["xticks"] is None:
            pass
        elif params["xticks"][0] is None:
            axe.set_xticklabels([])
        elif params["xticks"][a][b] is None:
            axe.set_xticklabels([])
        elif len(params["xticks"][a][b]) == 1:
            axe.set_xticks(params["xticks"][a][b][0])
        else:
            print(params["xticks"][a][b])
            axe.set_xticks(params["xticks"][a][b][0])
            axe.set_xticklabels(params["xticks"][a][b][1])

        if params["yticks"] is None:
            pass
        elif params["yticks"][0] is None:
            axe.set_yticks([])
        elif params["yticks"][a][b] is None:
            axe.set_yticks([])
        elif len(params["yticks"][a][b]) == 1:
            axe.set_yticks(params["yticks"][a][b][0])
        else:
            axe.set_yticks(params["yticks"][a][b][0])
            axe.set_yticklabels(params["yticks"][a][b][1])

        if params["ticksize"] is not None:
            axe.tick_params(axis="both", labelsize=params["ticksize"])

        if params["xlim"] is None:
            xmin, xmax = plt.xlim()
            axe.set_xlim([xmin, xmax])
        elif params["xlim"][0] is None:
            xmin, xmax = plt.xlim()
            axe.set_xlim([xmin, xmax])
        elif params["xlim"][a][b] is None:
            xmin, xmax = plt.xlim()
            axe.set_xlim([xmin, xmax])
        else:
            axe.set_xlim(params["xlim"][a][b])

        if params["ylim"] is None:
            ymin, ymax = plt.ylim()
            axe.set_ylim([ymin, ymax])
        elif params["ylim"][0] is None:
            ymin, ymax = plt.ylim()
            axe.set_ylim([ymin, ymax])
        elif params["ylim"][a][b] is None:
            ymin, ymax = plt.ylim()
            axe.set_ylim([ymin, ymax])
        else:
            axe.set_ylim(params["ylim"][a][b])

        condition = (params["xlabels"] is not None and
                     params["xlabels"][0] is not None and
                     params["xlabels"][a][b] is not None and
                     isinstance(params["xlabels"][a][b], str))
        if condition:
            axe.set_xlabel(params["xlabels"][a][b],
                           fontsize=params["labelsize"])

        condition = (params["ylabels"] is not None and
                     params["ylabels"][0] is not None and
                     params["ylabels"][a][b] is not None and
                     isinstance(params["ylabels"][a][b], str))

        if condition:
            ylabel = params["ylabels"][a][b]
            for var in printVariables:
                ylabel = re.sub(r"\[&%s\]" % var, printVariables[var], ylabel)
            axe.set_ylabel(ylabel,
                           fontsize=params["labelsize"])

        if params["legend_loc"] is not None:
            axe.legend(loc=params["legend_loc"],
                       fontsize=params["legend_fontsize"])


def plotLine(axe, position, params):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    plt.sca(axe)
    if len(position) == 1:
        ymin, ymax = plt.ylim()
    else:
        ymin, ymax = position[1][0], position[1][1]
        axe.plot(position[0], [ymin, ymax], color="black",
                 linestyle="--", linewidth=1)


def plotVerticalLine(axe, xpos, params):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    plt.sca(axe)
    ymin, ymax = plt.ylim()
    plotLine(axe, [[xpos, xpos], [ymin, ymax]], params)
    axe.set_ylim([ymin, ymax])


def plotPath(ax, a, b, obj, freq, params):
    if not isinstance(obj, PhysObj):
        raise TypeError("Error: obj must be of type PhysObj.")
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    if params["subplots"][0] == 1 and params["subplots"][1] == 1:
        axe = ax
    elif params["subplots"][0] == 1:
        axe = ax[b]
    elif params["subplots"][1] == 1:
        axe = ax[a]
    else:
        axe = ax[a, b]

    print('ax[%d, %d]' % (a, b), axe)
    print(params["indices"][a][b])
    for j, ll in enumerate(params["indices"][a][b]):
        if isinstance(params["linestyle"], list):
            if isinstance(params["linestyle"][0], list):
                linestyle = params["linestyle"][a][b][j]
            else:
                linestyle = params["linestyle"][j]
        else:
            linestyle = params["linestyle"]
        label = params["plot_labels"][a][b][j]
        if label is not None:
            axe.plot(np.arange(len(obj[:, freq, ll[0], ll[1]])),
                     obj[:, freq, ll[0], ll[1]], label=r"%s" % label,
                     marker="",
                     color=params["plot_colors"][a][b][j],
                     linestyle=linestyle, linewidth=params["linewidth"])
        else:
            axe.plot(np.arange(len(obj[:, freq, ll[0], ll[1]])),
                     obj[:, freq, ll[0], ll[1]], marker="",
                     color=params["plot_colors"][a][b][j],
                     linestyle=linestyle, linewidth=params["linewidth"])


def plotGrid(ax, a, b, obj, freq, mesh, params):
    if not isinstance(obj, PhysObj):
        raise TypeError("Error: obj must be of type PhysObj.")
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    if params["subplots"][0] == 1 and params["subplots"][1] == 1:
        axe = ax
    elif params["subplots"][0] == 1:
        axe = ax[b]
    elif params["subplots"][1] == 1:
        axe = ax[a]
    else:
        axe = ax[a, b]

    obj2 = obj.ReturnInPlane(mesh, [params["indices"][a][b], freq])

    if params["bounds"][0] is None:
        b0 = np.amin(obj2[np.nonzero(obj2)])
    else:
        b0 = params["bounds"][0]
    if params["bounds"][1] is None:
        b1 = np.amax(obj2[np.nonzero(obj2)])
    else:
        b1 = params["bounds"][1]

    bounds = [b0, b1]

    if isinstance(params["cmap"], str):
        cmap = params["cmap"]
    else:
        cmap = params["cmap"][a][b]

    if cmap == "opt1":
        if np.amin(bounds) >= 0:
            cmap = "Blues"
        elif np.amax(bounds) <= 0:
            cmap = "Reds_r"
        else:
            raise ValueError("NEED A NEW OPTION HERE")

    image = axe.imshow(obj2, origin="lower",
                       cmap=plt.get_cmap(cmap),
                       vmin=bounds[0], vmax=bounds[1])

    axes = inset_axes.inset_axes(axe, width="3%", height="90%", loc=2)
    bar = plt.colorbar(image, cax=axes, format=params["bar_format"][a][b])
    bar.ax.tick_params(labelsize=params["bar_ticksize"])
    bar.set_ticks([np.amin(obj2[np.nonzero(obj2)]),
                   np.amax(obj2[np.nonzero(obj2)])])


def plotGrid2(ax, a, b, objs, freq, mesh, params, printVariables=None,
              nophysobj=False, bounds=None, flag_colorbar=0):
    if printVariables is None:
        printVariables = {}
    if len(objs) not in [1, 2]:
        raise ValueError("Error: objs must be a length 1 or 2 array.")
    if (((not isinstance(objs[0], PhysObj) or not isinstance(objs[1], PhysObj))
         and not nophysobj)):
        raise TypeError("Error: objs[0/1] must be of type PhysObjs.")
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    if params["subplots"][0] == 1 and params["subplots"][1] == 1:
        axe = ax
    elif params["subplots"][0] == 1:
        axe = ax[b]
    elif params["subplots"][1] == 1:
        axe = ax[a]
    else:
        axe = ax[a, b]

    if len(objs) == 2:
        axe.axis("off")
        if not nophysobj:
            obj1 = objs[0].ReturnInPlane(mesh, [params["indices"][a][b], freq])
            obj2 = objs[1].ReturnInPlane(mesh, [params["indices"][a][b], freq])
        else:
            obj1 = objs[0]
            obj2 = objs[1]
    elif len(objs) == 1:
        if not nophysobj:
            obj1 = objs[0].ReturnInPlane(mesh, [params["indices"][a][b], freq])
        else:
            obj1 = objs[0]

    if len(objs) == 2:
        if not bounds:
            bounds1 = [np.amin(obj1),
                       np.amax(obj1)]
            bounds2 = [np.amin(obj2),
                       np.amax(obj2)]
        else:
            bounds1 = bounds[0]
            bounds2 = bounds[1]
    elif len(objs) == 1:
        if not bounds:
            bounds1 = [np.amin(obj1),
                       np.amax(obj1)]
        else:
            bounds1 = bounds[0]

    if len(objs) == 2:
        abs1 = np.amax([abs(bounds1[0]), bounds1[1]])
        abs2 = np.amax([abs(bounds2[0]), bounds2[1]])
    elif len(objs) == 1:
        abs1 = np.amax([abs(bounds1[0]), bounds1[1]])

    if isinstance(params["cmap"], colors.LinearSegmentedColormap):
        cmap = params["cmap"]
    elif isinstance(params["cmap"], str):
        cmap = plt.get_cmap(params["cmap"])
    else:
        cmap = plt.get_cmap(params["cmap"][a][b])

    if len(objs) == 2:
        a1 = inset_axes.inset_axes(axe, width="66%", height="66%", loc=2)
        a2 = inset_axes.inset_axes(axe, width="66%", height="66%", loc=4)
        a1.set_xticks([])
        a1.set_yticks([])
        a2.set_xticks([])
        a2.set_yticks([])
    elif len(objs) == 1:
        a1 = axe
        a1.set_xticks([])
        a1.set_yticks([])

    cond = (params["xlabels"] is not None and params["xlabels"][a][b] is not
            None and len(params["xlabels"]) == 2)
    if cond:
        xlab1 = params["xlabels"][a][b][0]
        xlab2 = params["xlabels"][a][b][1]
        for var in printVariables:
            print(var)
            print(printVariables[var])
            xlab1 = re.sub(r"\[&%s\]" % var, printVariables[var], xlab1)
            xlab2 = re.sub(r"\[&%s\]" % var, printVariables[var], xlab2)
        a1.set_title(xlab1, fontsize=params["titlesize"])
        a2.set_title(xlab2, fontsize=params["titlesize"], y=-0.2)

    if len(objs) == 2:
        image1 = a1.imshow(obj1, origin="lower", cmap=cmap,
                           vmin=-abs1, vmax=abs1)
        image2 = a2.imshow(obj2, origin="lower", cmap=cmap,
                           vmin=-abs2, vmax=abs2)
    elif len(objs) == 1:
        image1 = a1.imshow(obj1, origin="lower", cmap=cmap,
                           vmin=-abs1, vmax=abs1)

    if not flag_colorbar:
        if len(objs) == 2:
            inset1 = inset_axes.inset_axes(a1, width="3%", height="90%", loc=2)
            inset2 = inset_axes.inset_axes(a2, width="3%", height="90%", loc=2)

            bar1 = plt.colorbar(image1, cax=inset1,
                                format=params["bar_format"][a][b])
            bar2 = plt.colorbar(image2, cax=inset2,
                                format=params["bar_format"][a][b])

            bar1.ax.tick_params(labelsize=params["bar_ticksize"])
            bar2.ax.tick_params(labelsize=params["bar_ticksize"])

            bar1.set_ticks([-abs1, abs1])
            bar2.set_ticks([-abs2, abs2])
        if len(objs) == 1:
            inset1 = inset_axes.inset_axes(a1, width="3%", height="90%", loc=2)

            bar1 = plt.colorbar(image1, cax=inset1,
                                format=params["bar_format"][a][b])

            bar1.ax.tick_params(labelsize=params["bar_ticksize"])

            bar1.set_ticks([-abs1, abs1])

    if len(objs) == 2:
        if params["special1"]:
            plotSpecial1(a1, mesh.n, params)
            plotSpecial1(a2, mesh.n, params)
        if params["special2"]:
            plotSpecial2(a1, mesh.n, params)
            plotSpecial2(a2, mesh.n, params)
    elif len(objs) == 1:
        if params["special1"]:
            plotSpecial1(a1, mesh.n, params)
        if params["special2"]:
            plotSpecial2(a1, mesh.n, params)


def plotSpecial1(axe, n, params):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    s1 = params["special1"]

    dx = n[0]/2/math.cos(math.atan(s1[0]/s1[2]))
    dy = n[1]/2/math.cos(math.atan(s1[1]/s1[2]))

    def f(x1, x2, y1, y2, x):
        return 1.*(y2-y1)/(x2-x1)*(x-x1)+y1

    def g(x1, x2, y1, y2, y):
        return 1.*(x2-x1)/(y2-y1)*(y-y1)+x1

    def f1(x):
        return f(n[0]-1, 2*n[0]-1, -1, n[1]-1, x)

    def g1(y):
        return g(n[0]-1, 2*n[0]-1, -1, n[1]-1, y)

    def f2(x):
        return f(n[0]-1, 2*n[0]-1, 2*n[1]-1, n[1]-1, x)

    def g2(y):
        return g(n[0]-1, 2*n[0]-1, 2*n[1]-1, n[1]-1, y)

    def f3(x):
        return f(-1, n[0]-1, n[1]-1, -1, x)

    def g3(y):
        return g(-1, n[0]-1, n[1]-1, -1, y)

    def f4(x):
        return f(-1, n[0]-1, n[1]-1, 2*n[1]-1, x)

    def g4(y):
        return g(-1, n[0]-1, n[1]-1, 2*n[1]-1, y)

    # Inner vertical lines
    axe.plot([n[0]-1+dx, n[0]-1+dx],
             [f1(n[0]-1+dx), f2(n[0]-1+dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1+dx, n[0]-1+dx],
             [f1(n[0]-1+dx), f2(n[0]-1+dx)], color="black",
             linewidth=1, linestyle="--")

    axe.plot([n[0]-1-dx, n[0]-1-dx],
             [f3(n[0]-1-dx), f4(n[0]-1-dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1-dx, n[0]-1-dx],
             [f3(n[0]-1-dx), f4(n[0]-1-dx)], color="black",
             linewidth=1, linestyle="--")

    # Inner horizontal lines
    axe.plot([g3(n[1]-1-dy), g1(n[1]-1-dy)],
             [n[1]-1-dy, n[1]-1-dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g3(n[1]-1-dy), g1(n[1]-1-dy)],
             [n[1]-1-dy, n[1]-1-dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([g4(n[1]-1+dy), g2(n[1]-1+dy)],
             [n[1]-1+dy, n[1]-1+dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g4(n[1]-1+dy), g2(n[1]-1+dy)],
             [n[1]-1+dy, n[1]-1+dy], color="black",
             linewidth=1, linestyle="--")

    # Inner corners
    axe.plot([n[0]-1-dx, g3(n[1]-1-dy)],
             [f3(n[0]-1-dx), n[1]-1-dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1-dx, g3(n[1]-1-dy)],
             [f3(n[0]-1-dx), n[1]-1-dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([n[0]-1-dx, g4(n[1]-1+dy)],
             [f4(n[0]-1-dx), n[1]-1+dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1-dx, g4(n[1]-1+dy)],
             [f4(n[0]-1-dx), n[1]-1+dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([n[0]-1+dx, g1(n[1]-1-dy)],
             [f1(n[0]-1+dx), n[1]-1-dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1+dx, g1(n[1]-1-dy)],
             [f1(n[0]-1+dx), n[1]-1-dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([n[0]-1+dx, g2(n[1]-1+dy)],
             [f2(n[0]-1+dx), n[1]-1+dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1+dx, g2(n[1]-1+dy)],
             [f2(n[0]-1+dx), n[1]-1+dy], color="black",
             linewidth=1, linestyle="--")

    # Outer horizontal lines
    axe.plot([n[0]-1+dx, 2*n[0]-1],
             [f1(n[0]-1+dx), f1(n[0]-1+dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1+dx, 2*n[0]-1],
             [f1(n[0]-1+dx), f1(n[0]-1+dx)], color="black",
             linewidth=1, linestyle="--")

    axe.plot([n[0]-1+dx, 2*n[0]-1],
             [f2(n[0]-1+dx), f2(n[0]-1+dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([n[0]-1+dx, 2*n[0]-1],
             [f2(n[0]-1+dx), f2(n[0]-1+dx)], color="black",
             linewidth=1, linestyle="--")

    axe.plot([0, n[0]-1-dx],
             [f3(n[0]-1-dx), f3(n[0]-1-dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([0, n[0]-1-dx],
             [f3(n[0]-1-dx), f3(n[0]-1-dx)], color="black",
             linewidth=1, linestyle="--")

    axe.plot([0, n[0]-1-dx],
             [f4(n[0]-1-dx), f4(n[0]-1-dx)], color="white",
             linewidth=1, linestyle="-")
    axe.plot([0, n[0]-1-dx],
             [f4(n[0]-1-dx), f4(n[0]-1-dx)], color="black",
             linewidth=1, linestyle="--")

    # Outer vertical lines
    axe.plot([g3(n[1]-1-dy), g3(n[1]-1-dy)],
             [0, n[1]-1-dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g3(n[1]-1-dy), g3(n[1]-1-dy)],
             [0, n[1]-1-dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([g1(n[1]-1-dy), g1(n[1]-1-dy)],
             [0, n[1]-1-dy], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g1(n[1]-1-dy), g1(n[1]-1-dy)],
             [0, n[1]-1-dy], color="black",
             linewidth=1, linestyle="--")

    axe.plot([g4(n[1]-1+dy), g4(n[1]-1+dy)],
             [n[1]-1+dy, 2*n[1]-1], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g4(n[1]-1+dy), g4(n[1]-1+dy)],
             [n[1]-1+dy, 2*n[1]-1], color="black",
             linewidth=1, linestyle="--")

    axe.plot([g2(n[1]-1+dy), g2(n[1]-1+dy)],
             [n[1]-1+dy, 2*n[1]-1], color="white",
             linewidth=1, linestyle="-")
    axe.plot([g2(n[1]-1+dy), g2(n[1]-1+dy)],
             [n[1]-1+dy, 2*n[1]-1], color="black",
             linewidth=1, linestyle="--")


def plotSpecial2(axe, n, params):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")
    plotLine(axe, [[-1, n[0]-1], [n[1]-1, 2*n[1]-1]], params)
    plotLine(axe, [[-1, n[0]-1], [n[1]-1, -1]], params)
    plotLine(axe, [[n[0]-1, 2*n[0]-1], [-1, n[1]-1]], params)
    plotLine(axe, [[n[0]-1, 2*n[0]-1], [2*n[1]-1, n[1]-1]], params)


def finish(params, saveplace=None):
    if not isinstance(params, PlotParams):
        raise TypeError("Error: params must be of type PlotParams.")

    if params["tight_layout"] is True:
        plt.tight_layout()
    if saveplace is not None:
        plt.savefig(saveplace)
    if params["show"]:
        plt.show()
    else:
        plt.draw()
