from setuptools import setup
import json


# with open("requirements.txt") as f:
#     requirements = f.read().splitlines()


# setup(name="manager",
#       description="DMFT calculations project manager.",
#       install_requires=requirements,
#       )

setup()

# #########################
# # DATABASE INSTALLATION #
# #########################
# from src.manager.sql_db import Connector
# conn = Connector()
# db = conn.return_db_infos("tests")
# try:
#     from src.manager.sql_db import Connector
#     conn = Connector()
#     db = conn.return_db_infos("tests")
#     print("Could install correctly. Now setting tests database.")
# except Exception:
#     db = None
#     print("ERROR: Could not install correctly, at least the sql_db module.")
# try:
#     import sqlite3
#     import os
# except Exception:
#     raise Exception("sqlite3 or os modules does not work properly.")
#
# if db is None:
#     infos = {}
#     infos["name"] = "tests"
#     infos["ground_dir"] = (os.path.dirname(os.path.abspath(__file__))
#                            + "/manager/tests/dir_sql_db")
#     infos["db_path"] = infos["ground_dir"]
#     conn.mk_db_infos(infos)
#     conn.load(database="tests")
# else:
#     print("Database tests already exists.")
#     conn.load(database="tests")
#
# try:
#     from manager.Calculations import Barechi2
#     from manager.Calculations import Dressed
#     from manager.Calculations import Eliash
#     from manager.Calculations import DMFT
# except Exception:
#     raise Exception("Barechi2/Dressed/Eliash in manager.Calculations "
#                     "does not work properly.")
#
# ########################################################################
# # BARECHI2 OBJECTS: Create table and add a path and a grid calculation #
# ########################################################################
#
# # Create table barechi2
# b = Barechi2("system", conn)
# try:
#     b.CreateTable()
# except sqlite3.OperationalError:
#     print("Table barechi2 is already existing in tests database.")
#
# # Check/add/load barechi2 on grid (5x5x2)
# b_values = {"barechi2dir": "'/test_barechi2/Q_5x5x2/'",
#             "qpts": "'5x5x2'", "chempot": 6.9432565,
#             "niwn": 1024, "nivn": 2, "temp": 116,
#             "norb": 3, "nspin": 2, "status": "'done'"}
# b1id = conn.select("SELECT barechi2id FROM barechi2 WHERE barechi2dir=%s"
#                    % b_values["barechi2dir"])
# if not len(b1id):
#     b.InsertNew(b_values)
#     b1id = b.id
# else:
#     b1id = int(b1id[0][0])
#     print("Tests database: the barechi2 entry with q-grid"
#           " 5x5x2 is already there. Skip.")
# b1 = Barechi2("system", conn, id=b1id)
#
# # Check/add/load barechi2 on path
# b_values = {"barechi2dir": "'/test_barechi2/Qpath257/'",
#             "qpts": "'X-M-Ga-X-Z'", "chempot": 6.9432565, "nqpath": 257,
#             "niwn": 1024, "nivn": 2, "temp": 116,
#             "norb": 3, "nspin": 2, "status": "'done'"}
# b2id = conn.select("SELECT barechi2id FROM barechi2 WHERE barechi2dir=%s"
#                    % b_values["barechi2dir"])
# if not len(b2id):
#     b.InsertNew(b_values)
#     b2id = b.id
# else:
#     b2id = int(b2id[0][0])
#     print("Tests database: the barechi2 entry with q-path"
#           " on X-M-Ga-X-Z is already there. Skip.")
# b2 = Barechi2("system", conn, id=b2id)
#
# ###################################################################
# # DRESSED OBJECTS: Create table and a path and a grid calculation #
# ###################################################################
#
# # Create table dressed
# d = Dressed("system", conn)
# try:
#     d.CreateTable()
# except sqlite3.OperationalError:
#     print("Table barechi2 is already existing in tests database.")
#
# # Check/add/load dressed on grid (5x5x2)
# d_path = "/test_dressed/Q_5x5x2/"
# try:
#     os.makedirs(conn.groundDir + d_path)
# except Exception:
#     print("Directory for %s already created." % d_path)
#
# d1id = conn.select("SELECT dressedid FROM dressed WHERE dresseddir LIKE "
#                    "'" + d_path + "%" + "'")
# if not len(d1id):
#     d1 = b1.SetDressed()
#     d1.SetBaseDir(d_path)
#     d1_params = {"dressedU": 0.1, "dressedUp": 0.08, "dressedUpp": 0.07,
#                  "dressedJ": 0.01, "dressedJp": 0.01,
#                  "miwn": 64, "mivn": 2}
#     d1.SetParams(params=d1_params, verify=False)
#     d1.MakeTree()
#     d1.Load(True)
#
#     chi0 = b1.ReturnBareSusph(nkpt=100)
#     d1.Dressing(chi0=chi0)
#     d1id = d1.id
# else:
#     d1id = int(d1id[0][0])
#     print("Tests database: the dressed entry with q-grid"
#           " 5x5x2 is already there. Skip.")
# d1 = Dressed("system", conn, id=d1id)
#
# # Check/add/load dressed on path
# d_path = "/test_dressed/Qpath257/"
# try:
#     os.makedirs(conn.groundDir + d_path)
# except Exception:
#     print("Directory for %s already created." % d_path)
#
# d2id = conn.select("SELECT dressedid FROM dressed WHERE dresseddir LIKE "
#                    "'" + d_path + "%" + "'")
# if not len(d2id):
#     d2 = b2.SetDressed()
#     d2.SetBaseDir(d_path)
#     d2_params = {"dressedU": 0.1, "dressedUp": 0.08, "dressedUpp": 0.07,
#                  "dressedJ": 0.01, "dressedJp": 0.01,
#                  "miwn": 64, "mivn": 2}
#     d2.SetParams(params=d2_params, verify=False)
#     d2.MakeTree()
#     d2.Load(True)
#
#     chi0 = b2.ReturnBareSusph(nkpt=257)
#     d2.Dressing(chi0=chi0)
# else:
#     print("Tests database: the dressed entry with q-path"
#           " is already there. Skip.")
#
# #######################################################
# # ELIASH OBJECTS: Create table and a grid calculation #
# #######################################################
#
# # Create table eliash
# e = Eliash("system", conn)
# try:
#     e.CreateTable()
# except sqlite3.OperationalError:
#     print("Table eliash is already existing in tests database.")
#
# # Check/add eliash on grid
# e_path = "/test_eliash/Q_5x5x2/Fullspin_U-0.7_J-0.15_Up-0.4_Upp-0.25_Jp-0.15/"
#
# eid = conn.select("SELECT eliashid FROM eliash WHERE eliashdir='%s'" %
#                   e_path)
# if not len(eid):
#     # CAREFUL
#     # This is a way to add a calculation by forcing it by hand. Usually, this
#     # is not the correct way and it is better to follow the test
#     # named test_pipeline_01 in test_sql_db.
#     text_keys = ("(dressedid, eliashdir, parity, liwn, livn, liwn_max, nvec, "
#                  "eigs, syms, irreps, status)")
#     text_values = ("(%d, '%s', 0, 0, 1, 0, 3, NULL, NULL, NULL, 'launched')" %
#                    (d1id, e_path))
#     query = ("INSERT INTO eliash %s VALUES %s" % (text_keys, text_values))
#     eid = conn.insert(query)
#
#
# e_path = "/test_eliash/Q_5x5x2/Pseudo2_U-0.1_J-0.01_Up-0.08_Upp-0.07_Jp-0.01/"
#
# eid = conn.select("SELECT eliashdir FROM eliash WHERE eliashdir='%s'" %
#                   e_path)
# if not len(eid):
#     text_keys = ("(dressedid, eliashdir, parity, liwn, livn, liwn_max, nvec, "
#                  "eigs, syms, irreps, status)")
#     text_values = ("(%d, '%s', NULL, 0, 1, 0, 1, NULL, NULL, NULL, 'launched')"
#                    % (d1id, e_path))
#     query = ("INSERT INTO eliash %s VALUES %s" % (text_keys, text_values))
#     eid = conn.insert(query)
#
# ##############################
# # DMFT OBJECTS: Create table #
# ##############################
#
# # Create table dmft
# dm = DMFT("system", conn)
# try:
#     dm.CreateTable()
# except sqlite3.OperationalError:
#     print("Table dmft is already existing in the tests database.")
#
# # Check/add dmft calculation
# dm_path = "/test_dmft/test1/"
#
# dmid = conn.select("SELECT dmftid FROM dmft WHERE dmftdir='%s'" % dm_path)
#
# if not len(dmid):
#     beta, U, J = 100, 3, 0.5
#     solver, magnetic = "cthyb", "True"
#     params = {"general" :
#               {
#                "seedname" : "system",
#                "jobname" : "b%i-U%.1f-J%.2f" % (beta, U, J),
#                "csc" : "False",
#                "enforce_off_diag" : "True",
#                "#dft_mu" : 0,
#                "solver": solver,
#                "n_l" : 60,
#                "n_iw" : 2600,
#                "n_tau" : 26000,
#                "n_iter_dmft" : 1,
#                "measure_chi" : "NN",
#                "#measure_chi_SzSz" : "True",
#                "measure_chi_insertions" : 10,
#                "block_thershold" : 1e-03,
#                "h_int_type" : "kanamori",
#                "U" : U,
#                "J" : J,
#                "beta" : beta,
#                "prec_mu" : 0.001,
#                "g0_mix" : 0.7,
#                "dc_type" : 1,
#                "dc" : "True",
#                "dc_dmft" : "False",
#                "calc_energy" : "True",
#                "h5_save_freq" : 1,
#                "magnetic" : magnetic,
#               },
#               "advanced" :
#               {
#                "map_solver_struct" : ("{(\'ud_0\',0):(\'ud_0\',1),"
#                                       " (\'ud_0\',1):(\'ud_1\',1),"
#                                       " (\'ud_0\',2):(\'ud_0\',2),"
#                                       " (\'ud_0\',3):(\'ud_1\',2),"
#                                       " (\'ud_0\',4):(\'ud_1\',0),"
#                                       " (\'ud_0\',5):(\'ud_0\',0)}")
#               },
#               "solver" :
#               {
#                "length_cycle" : 500,
#                "n_warmup_cycles" : 50000,
#                "n_cycles_tot" : 1e6,
#                "imag_threshold" : 1e-6,
#                "measure_G_l" : "False",
#                "measure_G_tau" : "True",
#                "legendre_fit" : "True",
#                "#measure_density_matrix" : "True",
#                "measure_pert_order" : "True",
#               }
#              }
#
#     dm_values = {"dmftdir": dm_path,
#                  "beta": beta,
#                  "U": U,
#                  "J": J,
#                  "w90_in": "%ssystem.h5" % dm_path,
#                  "magnetic": magnetic,
#                  "solver": "cthyb",
#                  "parameters" : json.dumps(params),
#                 }
#
#     dm.InsertNew(dm_values)
#     dmid = dm.id
# else:
#     dmid = int(dmid[0][0])
#     print("Tests database: the dmft entry is already there. Skip.")
